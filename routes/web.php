<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');


Route::get('/cart/checkout', 'CartController@checkout')->name('cart.checkout');
Route::post('/cart/authenticate', 'CartController@authenticate')->name('cart.authenticate');
Route::post('/cart/register', 'CartController@register')->name('cart.register');



Route::middleware(['auth'])->group(function () {
    Route::post('/cart/pay', 'CartController@pay')->name('cart.pay');
    Route::post('/cart/makeOrder', 'CartController@makeOrder')->name('cart.makeOrder');
    Route::get('/cart/order/{id}', 'CartController@order')->name('cart.order');
    Route::get('/cart/orders', 'CartController@orders')->name('cart.orders');
    Route::get('/logout', 'HomeController@logout')->name('logout');

    Route::get('/admin/orders', 'AdminController@orders')->name('admin.orders');
    Route::get('/admin/order/{id}', 'AdminController@order')->name('admin.order');
});
Route::resource('cart', 'CartController');
Auth::routes();
