##Introduction
King of Kings Mining


## Installation
King of Kings Mining uses Laravel with a backend of MySQL.
You can read about how to install and configure the above from
their respective websites.

The following .env variables are necessary for proper running of the
software.

##### Lookups
SHOPPING_FORMAT_VALUES=true
MERCHANT_SECRET="provided by merchant"
MERCHANT_CUSTOMER_CODE="provided by merchant"
MERCHANT_SECRET_VAL="provided by merchant"
CURRENCY=USD

## Initial deployment commands
`php artisan migrate --step`  
`php artisan db:seed`


### After deployment instructions
`php artisan queue:restart`  

If it fails with Exception trace: 1 file_put_contents(" etc, run following commands

`sudo chmod -R gu+w storage`  
`sudo chmod -R guo+w storage`  
`php artisan cache:clear` 
`php artisan config:clear` 
`php artisan view:clear` 
