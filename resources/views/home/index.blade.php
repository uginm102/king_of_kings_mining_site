@extends('layouts.app')
@section('title', 'Home')
@section('content')
    <!-- Swiper-->
    <section id="home">
        <div class="swiper-container swiper-slider swiper-slider_fullheight bg-gray-dark" data-simulate-touch="false"
             data-loop="true" data-autoplay="4500">
            <div class="swiper-wrapper">
                <div class="swiper-slide" data-slide-bg="images/mining1.jpg">
                    <div class="swiper-slide-caption text-center">
                        <div class="container">
                            <div class="row justify-content-lg-center">
                                <div class="col-lg-10">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide text-center" data-slide-bg="images/mining1.png">
                    <div class="swiper-slide-caption">
                        <div class="container">
                            <div class="row justify-content-lg-center">
                                <div class="col-lg-10">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Swiper Pagination-->
            <div class="swiper-pagination"></div>
            <!-- Swiper Navigation-->
            <div class="swiper-button-prev linearicons-chevron-left"></div>
            <div class="swiper-button-next linearicons-chevron-right"></div>
        </div>
    </section>

    <!-- Our History-->
    <section class="novi-background bg-cover section-md bg-gray-lighter" id="about">
        <div class="container">
            <div class="row justify-content-md-center row-30 row-md-50">
                <div class="col-md-11 col-lg-10 col-xl-6">
                    <h4 class="heading-decorated">About Us</h4>
                    <p class="heading-6 text-width-1">We are the leading mining and exporting company of precious
                        minerals in the Great Lakes Region. We major in gold, diamond, tungsten, zinc, chromite, tin,
                        iron, lead, platinum, silver, manganese and many others.</p>

                    <p>For the last ten years we have moved steadily to maintain high standards of corporate governance
                        to achieve balance between profit, community growth, social equity and environmental
                        sustainability as we do business.</p>
                    <p>
                        We provide a safe and healthy environment for all our employees in a non-discriminatory and we
                        are so passionate to improve the lives of those in othe communities where we work.</p>
                </div>
                <div class="col-md-11 col-lg-10 col-xl-6"><img src="images/Diamond_Mine.jpg" alt="" width="652"
                                                               height="491"/>
                </div>
            </div>
        </div>
    </section>

    <!-- Our Services-->
    <section class="novi-background bg-cover section-md bg-default text-center" id="products">
        <div class="container">
            <div class="row" style="margin-bottom: 20px;">
                <div class="col-sm-12">
                    <h4 class="heading-decorated">Products</h4>
                </div>
            </div>
            <div class="row row-50 justify-content-md-center justify-content-lg-start">
                @forelse ($products as $product)
                    @include('home.product', ['product' => $product])
                @empty
                    @include('shared.empty',['message' => 'No products.'])
                @endforelse
            </div>
        </div>
    </section>

    <!-- Executive managers-->
    <section class="novi-background bg-cover section-lg bg-default text-center" id="contacts">
        <div class="container">
            <h4 class="heading-decorated">Executive managers</h4>
            <!-- Owl Carousel-->
            <div class="owl-carousel offset-top-1" data-items="1" data-sm-items="2" data-md-items="2" data-lg-items="2"
                 data-xl-items="2" data-dots="true" data-nav="false" data-stage-padding="15" data-margin="30"
                 data-mouse-drag="false" data-autoplay="false">
                <article class="thumb-flat"><img class="thumb-flat__image" src="images/director1.jpeg" alt=""
                                                 width="418" height="415"/>
                    <div class="thumb-flat__body">
                        <p class="heading-6"><a href="#">Benson Byaruhanga</a></p>
                        <p class="thumb-flat__subtitle">Director</p>
                        <p>...</p>
                    </div>
                </article>
                <article class="thumb-flat"><img class="thumb-flat__image" src="images/director21.jpeg" alt=""
                                                 width="418" height="415"/>
                    <div class="thumb-flat__body">
                        <p class="heading-6"><a href="#">John Koafias</a></p>
                        <p class="thumb-flat__subtitle">Director</p>
                        <p>...</p>
                    </div>
                </article>
            </div>
        </div>
    </section>

    <!-- contacts-->
    <section class="novi-background bg-cover section-md bg-default" id="contacts">
        <div class="container">
            <div class="row row-50">
                <div class="col-md-5 col-lg-4">
                    <h4 class="heading-decorated">Contact Details</h4>
                    <ul class="list-sm contact-info">
                        <li>
                            <dl class="list-terms-inline">
                                <dt>Address</dt>
                                <dd>Plot 12 Luthuri Avenue. P.O.Box 2000 Kampala.</dd>
                            </dl>
                        </li>
                        <li>
                            <dl class="list-terms-inline">
                                <dt>Phones</dt>
                                <dd>
                                    <ul class="list-semicolon">
                                        <li><a href="tel:#">(+256) 772-722-132</a></li>
                                    </ul>
                                </dd>
                            </dl>
                        </li>
                        <li>
                            <dl class="list-terms-inline">
                                <dt>We are open</dt>
                                <dd>Mn-Fr: 10 am-8 pm</dd>
                            </dl>
                        </li>
                        <li>
                            <ul class="list-inline-sm">
                                <li><a class="icon-sm fa-facebook novi-icon icon" href="#"></a></li>
                                <li><a class="icon-sm fa-twitter novi-icon icon" href="#"></a></li>
                                <li><a class="icon-sm fa-google-plus novi-icon icon" href="#"></a></li>
                                <li><a class="icon-sm fa-vimeo novi-icon icon" href="#"></a></li>
                                <li><a class="icon-sm fa-youtube novi-icon icon" href="#"></a></li>
                                <li><a class="icon-sm fa-pinterest-p novi-icon icon" href="#"></a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="col-md-7 col-lg-8">
                    <h4 class="heading-decorated">Get in Touch</h4>
                    <!-- RD Mailform-->
                    <form class="rd-mailform rd-mailform_style-1" data-form-output="form-output-global"
                          data-form-type="contact" method="post" action="bat/rd-mailform.php">
                        <div class="form-wrap form-wrap_icon">
                            <input class="form-input" id="contact-name" type="text" name="name"
                                   data-constraints="@Required">
                            <label class="form-label" for="contact-name">Your name</label><span
                                class="icon novi-icon linearicons-man"></span>
                        </div>
                        <div class="form-wrap form-wrap_icon">
                            <input class="form-input" id="contact-email" type="email" name="email"
                                   data-constraints="@Email @Required">
                            <label class="form-label" for="contact-email">Your e-mail</label><span
                                class="icon novi-icon linearicons-envelope"></span>
                        </div>
                        <div class="form-wrap form-wrap_icon">
                            <textarea class="form-input" id="contact-message" name="message"
                                      data-constraints="@Required"></textarea>
                            <label class="form-label" for="contact-message">Your message</label><span
                                class="icon novi-icon linearicons-feather"></span>
                        </div>
                        <button class="button button-primary" type="submit">send</button>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
