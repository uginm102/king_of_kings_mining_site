<div class="col-md-6 col-xl-4">
    <!-- Blurb circle-->
    <article class="blurb blurb-circle">
        <div class="unit flex-sm-row unit-spacing-md">
            <div class="unit__left">
                <div class="blurb-circle__icon"><span class="icon novi-icon novi-background linearicons-diamond"></span></div>
            </div>
            <div class="unit__body">
                <p class="heading-6 blurb__title"><a href="#">{{ $product->name }}</a></p>
                <p class="">
                    <a href="{{route('cart.create',['product_id' => $product->id])}}">
                        <i class="fas fa-shopping-cart"></i> &nbsp;
                        Add to Order
                    </a>
                </p>
                <span> {{ $product->priceDisp() }} {{ $product->currency }} per {{ $product->unit }} </span>
            </div>
        </div>
    </article>
</div>
