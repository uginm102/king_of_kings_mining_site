@extends('layouts.modal-app')
@section('modal-content')
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title">Assign role to user</h4>
        </div>
        {!! Form::model($user,['route' => ['admin.users.assignRole',$user], 'method' => 'post',
        'class' => 'form-columns', 'id' => 'modal-form' ]) !!}
        {{ csrf_field() }}
        <div class="modal-body">

            <div class="alert alert-danger {{ $errors->any() ? '' : 'hidden' }}">
                <ul>
                    @foreach ($errors->all() as $message)
                        <li>{{$message}}</li>
                    @endforeach
                </ul>
            </div>
            <div class="form-group {{ $errors->has('role_ids') ? 'has-error' : '' }}">
                {!!  Form::label('role_ids', __('user.role'), ['class' => '']) !!}
                {!!  Form::select('role_ids[]', $roles->pluck('name','id'), $user->roles->pluck('id'),
                [ 'class' => 'select2', 'multiple', 'placeholder' => '' ]) !!}
            </div>
            <i><b>Note*</b> to select many, press and hold down the control key and select</i>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            {!! Form::submit('Save', ['class' => "btn btn-primary"]) !!}
        </div>
        {!! Form::close() !!}
    </div><!-- /.modal-content -->
@endsection
@section('js')

    <script type="text/javascript">
        $(function () {
            main.initSelect2Modal();
        });
    </script>
@endsection


