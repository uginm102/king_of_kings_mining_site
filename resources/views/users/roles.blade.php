@if(!$user->getRoleNames()->isEmpty())
    <table class="table table-hover data-table-roles">
        <thead>
        <tr>
            <th class="col-md-11">
                Name
            </th>
            <th class="col-md-1">
                <span class="line"></span>
            </th>
        </tr>
        </thead>
        <tbody>
        <!-- row -->
        @foreach($user->roles as $role)
            <tr class="{{$loop->first ? 'first' : ''}}">
                <td>
                    {{$role->name}}
                </td>
                <td>
                    <a data-ajax="true"
                       href="{{route('admin.users.removeRole',[$user, $role])}}">
                        <i class="far fa-trash-alt"></i>
                    </a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
@else
    @include('shared.empty',['message' => 'No roles found.'])
@endif
<script type="text/javascript">
    $(function () {

        $('.data-table-roles').dataTable({
            "sPaginationType": "full_numbers",
            'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': ['nosort']
            }]
        });
        main.initAjax();
    });
</script>