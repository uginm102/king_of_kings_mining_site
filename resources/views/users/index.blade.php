@extends('layouts.app')
@section('title', 'Users')

@section('head-css')
    <!-- this page specific styles -->

    <link href="{{ mix('css/datatables.css') }}" rel="stylesheet" media="screen">
    <link href="{{ mix('css/list.css') }}" rel="stylesheet" type="text/css">

@endsection

@section('content')


<div class="row">
        <div class="col-lg-12">

                <div class="card-header bg-dark text-white">
                        <div class="card-widgets text-light">
                      
                        </div>
                        <h5 class="card-title mb-0 text-light">Users</h5>

                    </div>

        </div></div>


        <div class="row">
                <div class="col-lg-12">

<div class="row">
        <div class="col-xl-12 col-lg-12 nopadding" >
                <div class="card-box rowouter">
                   
        
                        <div class="row rowinner">
                                <div class="col-xl-12">
                                     

                                            <div class="card-box" id="clicked">
                                                    <div class="row">
                                                    <div class="col-md-12 panel">
                                                        <div class="row header">
                                                            
                                                                <div class="col-md-12 searchfilters">
                                                                    


                                                                        <div class="dropdown notification-list" style="float:right; margin-bottom:10px">
                                                                                <a class="btn btn-primary waves-effect waves-light dropdown-toggle " data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                                                                                    Search Filters <i class="fa fa-caret-down" aria-hidden="true"></i>
                                                                                </a>
                                       
                                                                               
                                                                        
                                                                                <div class="dropdown-menu dropdown-menu-right profile-dropdown">
                                                                                        <!-- item-->
                                                                                    
{!! Form::open(['route' => ['admin.users.index'], 'method' => 'get','class' => 'form-columns' ]) !!}  
                                                                                    
            <div class="col-md-12">

                <div class="form-group {{ $errors->has('q') ? 'has-error' : '' }}">
                    {!!  Form::label('q', __('general.search'), ['class' => '']) !!}
                    {!! Form::text('q',$q,['class' => "form-control", 'placeholder' => 'Search']) !!}

                </div>

                <div class="form-group {{ $errors->has('username') ? 'has-error' : '' }}">
                    {!!  Form::label('username', __('employee.username'), ['class' => '']) !!}
                    {!! Form::text('username',$username,['class' => "form-control", 'placeholder' => 'Search']) !!}

                </div>

                <div class="form-group {{ $errors->has('job_id') ? 'has-error' : '' }}">
                    {!!  Form::label('job_id', __('employee.job'), ['class' => '']) !!}
                    {!!  Form::select('job_id', $jobs->pluck('title','id'), $job_id,
                    [ 'class' => 'select2', 'placeholder' => '' ]) !!}
                </div>
                       
           
            
            </div>
                                    
                    
                    <div style="clear:both"></div>
                                                                        
<div class="dropdown-divider"></div>
<div class="col-md-12" style="margin-bottom: 5px;">
<div class="form-group actions" >

        <button type="submit" name="filter"  class="btn btn-primary waves-effect waves-light" id="btnfilter">Apply Filter</button>

        <a href="{{ route('admin.users.index') }}" class="btn btn btn-secondary waves-effect waves-light  btn-sm">Reset</a>
    

                                                                                           
                                                                              <div style="clear:both"></div>   
                                                                                        </div>          
                                                                                                
                                                                                        </div>

                                                                                        {!! Form::close() !!}
                                                                        
                                                                                    </div>
                                                                                </div>
                                                                        
                                                                                    </div>

        </div>
                  <!-- Users table -->
                  <div class="row">
                    <div class="col-md-12">
                        <table class="table table-hover" style="word-wrap:break-word;
                        table-layout: fixed;" id="requisition-list-override">
                            <thead>
                                <tr>
                                    <th class="col-md-4 sortable">
                                        Name
                                    </th>
                                    <th class="col-md-4 sortable">
                                        <span class="line"></span>Email
                                    </th>
                                    <th class="col-md-4 sortable">
                                        <span class="line"></span>Telephone
                                    </th>
                                   
                                </tr>
                            </thead>
                            <tbody>
                            <!-- row -->
                            @foreach ($users as $user)
                                               
                                                <tr class="{{ $loop->first ? 'first' : '' }} {{$loop->iteration  % 2 == 0 ? 'even' : 'odd'}}">
                                                    <td>
                                                        <a href="{{route('admin.users.show', $user)}}" class="name">
                                                            {{ $user->employee->full_name }}
                                                        </a>
                                                        <span class="subtext jobtitle">
                                                            {{$user->employee->activeJobTitle() }}
                                                        </span>
                                                    </td>
                                                    <td>
                                                    {{$user->email}}
                                                    </td>
                                                    <td>
                                                        {{ $user->employee->telephone }}
                                                    </td>
                                                   
                                                
                                                </tr>
                                             
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>

    <!-- end users table -->
    </div>

    </div></div>

</div></div></div></div></div>



                </div></div>

@endsection
@section('include-js')

<script type="text/javascript">
    $(function () {
        let data = {};
        data.url = "{{ route('cases.search') }}";

        main.initSelect2CaseFile(data);
    });
</script>
@endsection