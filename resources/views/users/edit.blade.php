@extends('layouts.app')
@section('title', 'Create role')

@section('head-css')
    <!-- this page specific styles -->
    {{--<link href="{{ asset('css/compiled/edit-create.css') }}" rel="stylesheet" media="screen">--}}
    <link href="{{ asset('css/compiled/detail-page.css') }}" rel="stylesheet" media="screen">
@endsection

@section('content')



                    <div class="row">



                            <div class="col-xl-6 col-lg-12 nopadding" >
                                    <div class="card-box rowouter">
                                     

<div class="row rowinner">
<div class="col-xl-12 nopadding">

        <div class="card-box rowouter">

                <div class="card-header bg-dark text-white">
                        <div class="card-widgets text-light">
                             
                        </div>
                        <h5 class="card-title mb-0 text-light">User</h5>
                    </div>

                <div class="card-box">
                        <div class="row">

            
                <div class="row rowinner">
        <div class="col-md-2">
                <img src="{{ asset('img/smartcase/employee-icon.png') }}" class="avatar img-circle" alt="contact"/>

        </div>

        <div class="col-md-7">
                <h3 class="name">{{ $user->employee->full_name}}</h3>
                <span class="area">{{ $user->email}}</span>
            </div>


         
                </div>
            
            
                        </div></div>
            
            </div>



                

            <div class="card-header bg-dark text-white">
                    <div class="card-widgets text-light">
                      
                    </div>
                    <h5 class="card-title mb-0 text-light">Roles</h5>
                </div>



                <div class="card-box">
                    

                                        @include('admin.users.form')
                                  



                       </div>
               

</div></div></div></div>


                   
                
                
                
         
                    <div class="col-xl-4 col-lg-8 nopadding" >
                            <div class="card-box rowouter">
                               
                               <div class="row rowinner">
                                    <div class="col-xl-12 actions">


                                        
                                    </div></div></div></div>
                
                
                
                
                
                </div>






@endsection

@section('include-js')

<script src="{{ asset('assets/js/app.min.js') }}"></script>

@endsection

@section('js')
    <script type="text/javascript">
        $(function () {

        });
    </script>
@endsection