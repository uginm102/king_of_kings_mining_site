@extends('layouts.modal-app')
@section('size', 'modal-lg')
@section('modal-content')
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title">Assign permission to user</h4>
        </div>
        {!! Form::model($user,['route' => ['admin.users.assignPermission',$user], 'method' => 'post',
        'class' => 'form-columns', 'id' => 'modal-form' ]) !!}
        {{ csrf_field() }}
        <div class="modal-body">

            <div class="alert alert-danger {{ $errors->any() ? '' : 'hidden' }}">
                <ul>
                    @foreach ($errors->all() as $message)
                        <li>{{$message}}</li>
                    @endforeach
                </ul>
            </div>
            <div class="form-group {{ $errors->has('permission_ids') ? 'has-error' : '' }}">
               
                {!!  Form::select('permission_ids[]', $permissions->pluck('name','id'), $user->permissions->pluck('id'),
                [ 'class' => 'select2', 'multiple', 'placeholder' => '' ]) !!}
            </div>
            <i><b>Note*</b> to select many, press and hold down the control key and select</i>
        </div>
        <div class="modal-footer">
          
            {!! Form::submit('Save', ['class' => "btn btn-primary waves-effect waves-light"]) !!}
            <button type="button" class="btn btn-secondary waves-effect waves-light" data-dismiss="modal">Close</button>
        </div>
        {!! Form::close() !!}
    </div><!-- /.modal-content -->
@endsection
@section('js')

    <script type="text/javascript">
        $(function () {
            main.initSelect2Modal();
        });
    </script>
@endsection


