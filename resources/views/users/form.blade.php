@if( $user->id > 0 )
    {!! Form::model($user, ['route' => ['admin.users.update', $user],
    'method' => 'put', 'class' => 'form-columns' ]) !!}
@else
    {!! Form::open(['route' => 'admin.users.store', 'method' => 'post', 'class' => 'form-columns']) !!}
@endif
{{ csrf_field() }}
{{--{{session()->get('e') }}--}}
<div class="row">
    <div class="col-md-12">
        <div class="alert alert-danger {{ $errors->any() ? '' : 'hidden' }}">
            <ul>
                @foreach ($errors->all() as $message)
                    <li>{{$message}}</li>
                @endforeach
            </ul>
        </div>
        <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
            {!!  Form::label('email', __('general.email'), ['class' => '']) !!}
            {!! Form::text('email',$user->email,
            ['class' => "form-control", 'placeholder' => __('employee.email_placeholder')]) !!}
        </div>

       <div class="form-group {{ $errors->has('is_active') ? 'has-error' : '' }}">
           
        {!! Form::checkbox('is_active', 'yes', true,['class' => 'mgc mgc-primary']); !!}
        {!!  Form::label('is_active', __('Is active'), ['class' => '']) !!}
    </div>
    </div>
    

   
        <div class="col-md-12">
            <div class="form-group clearfix actions">
                {!! Form::submit(__('general.submit'), ['class' => "btn btn-primary"]) !!}
                @if( $user->id > 0 )
                    <a href="{{ route('admin.users.show', $user) }}"
                       class="btn btn-secondary">{{ __('general.cancel') }}</a>
                @else
                    <a href="{{ route('admin.users.index') }}"
                       class="btn btn-secondary">{{ __('general.cancel') }}</a>
                @endif
            </div>
        </div>
 
</div>
{!! Form::close() !!}