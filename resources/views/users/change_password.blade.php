@extends('layouts.app')
@section('title', 'Personal information')

@section('head-css')
    <!-- this page specific styles -->
    <link href="{{ asset('css/compiled/detail-page.css') }}" rel="stylesheet" media="screen">
@endsection

@section('content')
    <div id="pad-wrapper" class="detail">
        <!-- header -->
        <div class="row header">
            <div class="col-md-8">
                <img src="{{ asset('img/smartcase/employee-icon.png') }}" class="avatar img-circle" alt="contact"/>
                <h3 class="name">{{ $user->employee->full_name}}</h3>
                <span class="area">{{ $user->email}}</span>
            </div>
        </div>

        <div class="row profile">
            <!-- bio, new note & orders column -->
            <div class="col-md-9 bio">
                <div class="profile-box">
                    <div class="row">
                        <div class="col-md-6 section clearfix">
                            <div class="row">
                                <div class="col-md-12">
                                    {!! Form::model($user, ['route' => ['admin.users.changePasswordSubmit'],
                                        'method' => 'post', 'class' => 'form-columns', 'id' => 'change-password-form' ]) !!}
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="alert alert-danger {{ $errors->any() ? '' : 'hidden' }}">
                                                <ul>
                                                    @foreach ($errors->all() as $message)
                                                        <li>{{$message}}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                            <div class="form-group {{ $errors->has('old_password') ? 'has-error' : '' }}">
                                                {!!  Form::label('old_password', __('user.old_password'), ['class' => '']) !!}
                                                {!! Form::password('old_password', ['class' => "form-control", 'placeholder' => __('user.old_password')]) !!}
                                            </div>

                                            <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                                                {!!  Form::label('password', __('user.password'), ['class' => '']) !!}
                                                {!! Form::password('password', ['class' => "form-control", 'placeholder' => __('user.password')]) !!}
                                            </div>
                                            <div class="form-group {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
                                                {!!  Form::label('password_confirmation', __('user.password_confirmation'), ['class' => '']) !!}
                                                {!! Form::password('password_confirmation', ['class' => "form-control", 'placeholder' => __('user.password_confirmation')]) !!}
                                            </div>

                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group clearfix">
                                                    {!! Form::submit(__('general.submit'), ['class' => "btn btn-primary pull-right"]) !!}
                                                    <a href="{{ route('admin.users.personalInfo') }}"
                                                       class="btn btn-link pull-right">{{ __('general.cancel') }}</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- side address column -->
            <div class="col-md-3 col-xs-12 address pull-right">
            </div>
        </div>
    </div>
@endsection

@section('include-js')
    <script src="{{ asset('js/jquery.validate.js') }}"></script>
@endsection

@section('js')
    <script type="text/javascript">
        $(function () {
            $( "#change-password-formq" ).validate( {
                rules: {
                    old_password: "required",
                    password: {
                        required: true,
                        minlength: 5
                    },
                    password_confirmation: {
                        required: true,
                        minlength: 5,
                        equalTo: "#password"
                    },
                    agree: "required"
                },
                messages: {
                    old_password: "Please enter your old password",
                    password: {
                        required: "Please provide a password",
                        minlength: "Your password must be at least 5 characters long"
                    },
                    password_confirmation: {
                        required: "Please provide a password",
                        minlength: "Your password must be at least 5 characters long",
                        equalTo: "Please enter the same password as above"
                    },
                    agree: "Please accept our policy"
                },
                errorElement: "em",
                errorPlacement: function ( error, element ) {
                    // Add the `help-block` class to the error element
                    error.addClass( "help-block" );

                    if ( element.prop( "type" ) === "checkbox" ) {
                        error.insertAfter( element.parent( "label" ) );
                    } else {
                        error.insertAfter( element );
                    }
                },
                highlight: function ( element, errorClass, validClass ) {
                    $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                },
                unhighlight: function (element, errorClass, validClass) {
                    $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                }
            } );
        });
    </script>
@endsection