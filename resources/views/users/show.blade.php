@extends('layouts.app')
@section('title', 'User')

@section('head-css')
    <!-- this page specific styles -->
    <link href="{{ asset('css/compiled/detail-page.css') }}" rel="stylesheet" media="screen">
    <link href="{{ mix('css/datatables.css') }}" rel="stylesheet" media="screen">
@endsection

@section('content')
    <div class="row">
        <div class="col-xl-8 col-lg-12 nopadding">
            <div class="card-box rowouter">
                <div class="row rowinner">
                    <div class="col-xl-12">

                        <div class="card-box rowouter">

                            <div class="card-header bg-dark text-white">
                                <div class="card-widgets text-light">

                                </div>
                                <h5 class="card-title mb-0 text-light">User</h5>
                            </div>

                            <div class="card-box">
                                <div class="row">
                                    <div class="row rowinner">
                                        <div class="col-md-2">
                                            <img src="{{ asset('img/smartcase/employee-icon.png') }}"
                                                 class="avatar img-circle" alt="contact"/>
                                        </div>
                                        <div class="col-md-7">
                                            <h3 class="name">{{ $user->employee->full_name}}</h3>
                                            <span class="area">{{ $user->email}}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-header bg-dark text-white">
                            <div class="card-widgets text-light">
                                <a href="{{route('admin.users.assignRole', $user)}}" data-ajax="true" class="actionbtn">
                                    <i class="fas fa-cogs"></i>
                                    Manage
                                </a>
                            </div>
                            <h5 class="card-title mb-0 text-light">Roles</h5>
                        </div>
                        <div class="card-box">
                            <div id="reload-container-roles">
                                @include('admin.users.roles')
                            </div>
                        </div>
                        <div class="card-header bg-dark text-white">
                            <div class="card-widgets text-light">
                                <a href="{{route('admin.users.assignPermission', $user)}}" data-ajax="true"
                                   class="actionbtn">
                                    <i class="fas fa-cogs"></i>
                                    Manage
                                </a>
                            </div>
                            <h5 class="card-title mb-0 text-light">Permissions</h5>
                        </div>
                        <div class="card-box">
                            <div id="reload-container-permissions">
                                @include('admin.users.permissions')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-4 col-lg-8 nopadding">
            <div class="card-box rowouter">
                <div class="row rowinner">
                    <div class="col-xl-12 actions">
                        <a class="btn btn-primary spaced-right" href="{{ route('admin.users.edit', $user) }}">
                            Edit user
                        </a>
                        @if($locked)
                        <a data-ajax="true" class="btn btn-primary spaced-right"
                           href="{{route('admin.users.unlock', $user)}}">
                            Unlock user
                        </a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('include-js')
    <script src="{{ asset('js/moment.js') }}"></script>
    <script src="{{ mix('js/datatables.js') }} }}"></script>
    <script src="{{ asset('assets/js/app.min.js') }}"></script>
@endsection
@section('js')
    <script type="text/javascript">
        $(function () {

        });
    </script>
@endsection