@if(!$user->getDirectPermissions()->isEmpty())
    <table class="table table-hover data-table-permissions full-width-table" style="width:100%">
        <thead>
        <tr>
            <th class="col-md-11">
                Name
            </th>
            <th class="col-md-1">
                <span class="line"></span>
            </th>
        </tr>
        </thead>
        <tbody>
        <!-- row -->
        @foreach($user->permissions as $permission)
            <tr class="{{$loop->first ? 'first' : ''}}">
                <td>
                    {{$permission->name}}
                </td>
                <td>
                    <a data-ajax="true"
                       href="{{route('admin.users.removePermission',[$user, $permission])}}">
                        <i class="far fa-trash-alt"></i>
                    </a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
@else
    @include('shared.empty',['message' => 'No permissions found.'])
@endif
<script type="text/javascript">
    $(function () {
        $('.data-table-permissions').dataTable({
            "sPaginationType": "full_numbers",
            'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': ['nosort']
            }]
        });
        main.initAjax();
    });
</script>