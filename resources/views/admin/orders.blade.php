@extends('layouts.cart-app')
@section('title', 'Orders')
@section('content')
    <div class="checkout" >
        <div class="row">
            <div class="col-md-9 offset-md-1">
                {!! Form::open(['route' => ['admin.orders'], 'method' => 'get','class' => 'form-columns' ]) !!}
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('q') ? 'has-error' : '' }}">
                            {!!  Form::label('q', __('general.search'), ['class' => '']) !!}
                            {!! Form::text('q',$q,['class' => "form-control", 'placeholder' => 'Search']) !!}
                        </div>
                    </div>

                </div>
                <div class="row margin-bottom-15">
                    <div class="col-md-12">
                        {!! Form::submit('Search', ['class' => "btn btn-primary waves-effect waves-light btn-sm"]) !!}
                        <a href="{{ route('admin.orders') }}"
                           class="btn btn-secondary waves-effect waves-light btn-sm ">Reset</a>
                    </div>
                </div>
                {!! Form::close() !!}

                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive-sm">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th style="width: 5%">#</th>
                                    <th style="width: 15%">Code</th>
                                    <th style="width: 15%">Date</th>
                                    <th style="width: 5%">Status</th>
                                    <th style="width: 25%">Customer</th>
                                    <th style="width: 35%; text-align: right;">Amount</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse ($orders as $order)
                                    <tr>
                                        <td >{{ $loop->index + 1 }}</td>
                                        <td >
                                            <a href="{{route('admin.order', $order)}}">
                                                {{ $order->code }}
                                            </a>
                                        </td>
                                        <td >{{ $order->date->toFormattedDateString() }}</td>
                                        <td>
                                            @if($order->is_cleared)
                                                <i class="far fa-check-circle" style="color: green;"></i>
                                            @else
                                                Pending
                                            @endif
                                        </td>
                                        <td>
                                            {{ $order->customer->full_name }}
                                        </td>
                                        <td class="right">{{ $order->amountDisp() }} {{ $order->currency }}</td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="4">
                                            @include('shared.empty',['message' => 'No orders found.'])
                                        </td>
                                    </tr>
                                @endforelse

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        {{ $orders->appends(request()->query())->render() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
