@extends('layouts.cart-app')
@section('title', 'Order')
@section('content')
    <div class="checkout" >
        <div class="row">
            <div class="col-md-10 offset-1">
                <div class="card">
                    <div class="card-header clearfix">
                        Order #: {{ $order->code }}

                        <div class="pull-right">
                            <a href="{{ route('admin.orders') }}">
                                Back to orders
                            </a>
                        </div>
                    </div>

                    <div class="card-body">
                        <div class="row" style="margin-bottom: 10px">
                            <div class="col-sm-4">
                                <h6 class="mb-3">Details:</h6>
                                <div>Order<strong>#: {{ $order->code }}</strong></div>
                                <div> Status:
                                    @if($order->is_cleared)
                                        <i class="far fa-check-circle" style="color: green;"></i>
                                    @else
                                        Pending
                                    @endif
                                </div>
                                <div>{{ $order->date->toFormattedDateString() }}</div>
                                <div> Customer: {{ $order->customer->full_name }}</div>
                                <div>E-mail: {{ $order->customer->email }}</div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive-sm">
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th style="width: 5%">#</th>
                                            <th style="width: 20%">Item</th>
                                            <th style="width: 15%">Code</th>
                                            <th style="width: 20%; text-align: right;">Quantity</th>
                                            <th style="width: 20%; text-align: right;">Unit Cost</th>
                                            <th style="width: 20%; text-align: right;">Total</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @forelse ($order->orderItems as $item)
                                            <tr>
                                                <td > {{ $loop->index + 1 }} </td>
                                                <td >{{ $item->name }}</td>
                                                <td >{{ $item->code }}</td>
                                                <td class="right">{{ $item->quantityDisp() }} {{ $item->unit }}</td>
                                                <td class="right">{{ $item->unitPriceDisp() }} {{ $order->currency}}</td>
                                                <td class="right">{{ $item->amountDisp() }} {{ $order->currency}}</td>
                                            </tr>
                                        @empty
                                            @include('shared.empty',['message' => 'Order has no items.'])
                                        @endforelse
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
