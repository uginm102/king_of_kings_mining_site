@extends('layouts.app')
@section('title', 'Roles')

@section('head-css')
    <!-- this page specific styles -->
    <link href="{{ asset('css/compiled/list.css') }}" rel="stylesheet" media="screen">
    <link href="{{ mix('css/datatables.css') }}" rel="stylesheet" media="screen">
@endsection

@section('content')


<div class="row">
        <div class="col-xl-8 col-lg-12 nopadding" >
            <div class="card-box rowouter">
             

<div class="row rowinner">
<div class="col-xl-12">   

<div class="card-header bg-dark text-white">
<div class="card-widgets text-light">
        <a href="{{route('admin.roles.create')}}" class="actionbtn">
                <span>&#43;</span>
                New role
            </a>
</div>
<h5 class="card-title mb-0 text-light">Roles</h5>
</div>


<div class="card-box">
<div class="row">
  <!-- Users table -->

        <div class="col-md-12">
            <table class="table table-hover data-table">
                <thead>
                <tr>
                    <th class="col-md-12 sortable">
                        Name
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach ($roles as $role)
                    <!-- row -->
                    <tr class="first">
                        <td>
                            <a href="{{route('admin.roles.show', $role)}}" class="text-uppercase">
                                {{ $role->name }}
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>


</div></div>

</div></div></div></div></div>






@endsection

@section('include-js')
    <script src="{{ asset('js/moment.js') }}"></script>


    <script src="{{ mix('js/datatables.js') }} }}"></script>

<script src="{{ asset('assets/js/app.min.js') }}"></script>
@endsection

@section('js')
    <script type="text/javascript">
        $(function () {
            $('.data-table').dataTable({
                "sPaginationType": "full_numbers",
                'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': ['nosort']
                }]
            });

            main.initAjax();
        });
    </script>
@endsection
