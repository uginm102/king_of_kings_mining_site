@if( $role->id > 0 )
    {!! Form::model($role, ['route' => ['admin.roles.update', $role],
    'method' => 'put', 'class' => 'form-columns' ]) !!}
@else
    {!! Form::open(['route' => 'admin.roles.store', 'method' => 'post', 'class' => 'form-columns full-width']) !!}
@endif
{{ csrf_field() }}
{{--{{session()->get('e') }}--}}

    
        <div class="alert alert-danger {{ $errors->any() ? '' : 'hidden' }}">
            <ul>
                @foreach ($errors->all() as $message)
                    <li>{{$message}}</li>
                @endforeach
            </ul>
        </div>
        <div class="col-md-12">
        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
            {!!  Form::label('name', 'Name', ['class' => '']) !!}
            {!! Form::text('name',$role->name,
            ['class' => "form-control", 'placeholder' => 'name']) !!}
        </div></div>

        <div class="col-md-12">
            <div class="form-group clearfix actions">
                {!! Form::submit(__('general.submit'), ['class' => "btn btn-primary waves-effect width-md waves-light "]) !!}
    
                @if( $role->id > 0 )
                    <a href="{{ route('admin.roles.show', $role) }}" class="btn btn-secondary waves-effect width-md waves-light ">Cancel</a>
                @else
                    <a href="{{ route('admin.roles.index') }}" class="btn btn-secondary waves-effect width-md waves-light ">Cancel</a>
                @endif
            </div>
    
    </div>

{!! Form::close() !!}