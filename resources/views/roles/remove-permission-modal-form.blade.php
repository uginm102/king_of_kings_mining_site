@extends('layouts.modal-app')
@section('modal-content')
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title">Remove permission from role</h4>
        </div>
        {!! Form::model($role,['route' => ['admin.roles.removePermissionSubmit',$role], 'method' => 'post',
        'class' => 'form-columns', 'id' => 'modal-form' ]) !!}
        {{ csrf_field() }}
        <div class="modal-body">
{!! Form::hidden('permission_id', $permission->id) !!}
            <div class="alert alert-danger {{ $errors->any() ? '' : 'hidden' }}">
                <ul>
                    @foreach ($errors->all() as $message)
                        <li>{{$message}}</li>
                    @endforeach
                </ul>
            </div>
            <div class="form-group {{ $errors->has('archive_status') ? 'has-error' : '' }}">
                {{$permission->name}}
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            {!! Form::submit('Save', ['class' => "btn btn-primary"]) !!}
        </div>
        {!! Form::close() !!}
    </div><!-- /.modal-content -->
@endsection
@section('js')

    <script type="text/javascript">
        $(function () {
        });
    </script>
@endsection


