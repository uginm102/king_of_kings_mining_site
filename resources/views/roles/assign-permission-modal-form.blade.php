@extends('layouts.modal-app')
@section('modal-content')
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title">Assign permission to role</h4>
        </div>
        {!! Form::model($role,['route' => ['admin.roles.assignPermission',$role], 'method' => 'post',
        'class' => 'form-columns', 'id' => 'modal-form' ]) !!}
        {{ csrf_field() }}
        <div class="modal-body">

            <div class="alert alert-danger {{ $errors->any() ? '' : 'hidden' }}">
                <ul>
                    @foreach ($errors->all() as $message)
                        <li>{{$message}}</li>
                    @endforeach
                </ul>
            </div>
            <div class="form-group {{ $errors->has('permission_ids') ? 'has-error' : '' }}">
                {!!  Form::label('permission_ids', 'Permission', ['class' => '']) !!}
                {!!  Form::select('permission_ids[]', $permissions->pluck('name','id'), $role->permissions->pluck('id'),
                [ 'class' => 'select2-modal', 'multiple',  'placeholder' => '' ]) !!}
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            {!! Form::submit('Save', ['class' => "btn btn-primary"]) !!}
        </div>
        {!! Form::close() !!}
    </div><!-- /.modal-content -->
@endsection
@section('js')
    <script type="text/javascript">
        $(function () {
            main.initSelect2Modal();
        });
    </script>
@endsection


