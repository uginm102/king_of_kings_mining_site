@extends('layouts.app')
@section('title', 'Create role')

@section('head-css')
    <!-- this page specific styles -->
    <link href="{{ asset('css/compiled/edit-create.css') }}" rel="stylesheet" media="screen">
@endsection

@section('content')

<div class="row">
        <div class="col-xl-5 col-lg-12 nopadding" >
            <div class="card-box rowouter">
             

<div class="row rowinner">
<div class="col-xl-12">   

<div class="card-header bg-dark text-white">
<div class="card-widgets text-light">
 
</div>
<h5 class="card-title mb-0 text-light">Create a new role</h5>
</div>


<div class="card-box">
<div class="row">
        @include('admin.roles.form')

</div></div>
</div></div></div></div></div>




@endsection

@section('include-js')
    <script src="{{ asset('js/bootstrap.datepicker.js') }}"></script>

<script src="{{ asset('assets/js/app.min.js') }}"></script>
@endsection
@section('js')
    <script type="text/javascript">
        $(function () {

        });
    </script>
@endsection