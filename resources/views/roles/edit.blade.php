@extends('layouts.app')
@section('title', 'Edit role')

@section('head-css')
    <!-- this page specific styles -->
    <link href="{{ asset('css/compiled/edit-create.css') }}" rel="stylesheet" media="screen">
@endsection

@section('content')
    <div id="pad-wrapper" class="edit-create">
        <div class="row header">
            <div class="col-md-12">
                <h3>Edit role</h3>
            </div>
        </div>

        <div class="row form-wrapper">
            <!-- left column -->
            <div class="col-md-6 with-sidebar">
                @include('admin.roles.form')
            </div>
            <!-- side right column -->
            <div class="col-md-3 form-sidebar">
            </div>
        </div>
    </div>
@endsection

@section('include-js')
    <script src="{{ asset('js/bootstrap.datepicker.js') }}"></script>
@endsection
@section('js')
    <script type="text/javascript">
        $(function () {

        });
    </script>
@endsection