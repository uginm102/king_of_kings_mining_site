@extends('layouts.app')
@section('title', 'Role')

@section('head-css')
    <!-- this page specific styles -->
    <link href="{{ asset('css/compiled/detail-page.css') }}" rel="stylesheet" media="screen">
    <link href="{{ mix('css/datatables.css') }}" rel="stylesheet" media="screen">
@endsection

@section('content')


<div class="row">
        <div class="col-xl-8 col-lg-12 nopadding" >
            <div class="card-box rowouter">
             

<div class="row rowinner">
<div class="col-xl-12">   

<div class="card-header bg-dark text-white">
<div class="card-widgets text-light">
 
</div>
<h5 class="card-title mb-0 text-light">Create a new role</h5>
</div>


<div class="card-box">
<div class="row">
        <div class="col-md-3">
                <img src="{{ asset('img/smartcase/shield.png') }}" class="avatar img-circle" alt="contact"/>
              
            </div>

            <div class="col-md-7">
                    <h3 class="name">{{$role->name}}</h3>
                </div>

</div></div>



<div class="card-header bg-dark text-white">
        <div class="card-widgets text-light">
                <a href="{{route('admin.roles.assignPermission', $role)}}" data-ajax="true" class="actionbtn">
                        <i class="fas fa-cogs"></i>
                        Manage
                    </a>
        </div>
        <h5 class="card-title mb-0 text-light">Permissions</h5>
        </div>
        
        
        <div class="card-box">
        <div class="row">
                <div class="col-md-12">
                <div id="reload-container-permissions">
                        @include('admin.roles.permissions')
                    </div></div>
        </div></div>



</div></div></div></div>


@endsection

@section('include-js')
    <script src="{{ asset('js/moment.js') }}"></script>
    <script src="{{ mix('js/datatables.js') }} }}"></script>

<script src="{{ asset('assets/js/app.min.js') }}"></script>
@endsection

@section('js')
    <script type="text/javascript">
        $(function () {
        });
    </script>
@endsection