@extends('layouts.cart-app')
@section('title', 'Orders')
@section('content')
    <div class="checkout" >
        <div class="row">
            <div class="col-md-7 offset-md-1">
                <div class="card">
                    <div class="card-header">Order for: {{ $user->full_name }}</div>

                    <div class="card-body">
                        <div class="row" style="margin-bottom: 10px">
                            <div class="col-sm-12">
                                <h6 class="mb-3">Details:</h6>
                                <div>E-mail: {{ $user->email }}</div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive-sm">
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th style="width: 5%">#</th>
                                            <th style="width: 30%">Code</th>
                                            <th style="width: 20%">Date</th>
                                            <th style="width: 5%">Status</th>
                                            <th style="width: 40%; text-align: right;">Amount</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @forelse ($orders as $order)
                                            <tr>
                                                <td >{{ $loop->index + 1 }}</td>
                                                <td >
                                                    <a href="{{route('cart.order', $order)}}">
                                                        {{ $order->code }}
                                                    </a>
                                                </td>
                                                <td >{{ $order->date->toFormattedDateString() }}</td>
                                                <td>
                                                    @if($order->is_cleared)
                                                    <i class="far fa-check-circle" style="color: green;"></i>
                                                    @else
                                                        Pending
                                                    @endif
                                                </td>
                                                <td class="right">{{ $order->amountDisp() }} {{ $order->currency }}</td>
                                            </tr>
                                        @empty
                                            <tr>
                                                <td colspan="4">
                                                    @include('shared.empty',['message' => 'No orders found.'])
                                                </td>
                                            </tr>
                                        @endforelse

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
