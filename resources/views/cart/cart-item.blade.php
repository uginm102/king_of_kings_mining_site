<div class="row {{ $loop->last ? 'last' : '' }}">
    <div class="col-md-12">
        <div class="cart_item">
            <div class="row">
                <div class="col-md-2">
                    <a href="#" class="cart_product_image">
                        <img src="{{ asset("images/product.png") }}" alt="" width="100" height="100"/>
                    </a>
                </div>
                <div class="col-md-10">
                    <div class="row">
                        <div class="col-md-11 item-title"><a href="#">{{ $item->name }}</a></div>
                        <div class="col-md-1">
                            {!! Form::model($item,['route' => ['cart.destroy', $item->id], 'method' => 'DELETE', 'class' => 'form-columns', 'id' => 'modal-form' ]) !!}
                            <button type="submit" class="btn btn-link delete">
                                <i class="fas fa-times"></i>
                            </button>
                            {!! Form::close() !!}
                        </div>
                    </div>

                    <div class="row" style="padding-top: 10px;">
                        <div class="col-md-2 item-code">Code: {{ $item->attributes['code_attr'] }}</div>
                        <div class="col-md-6">
                            {!! Form::model($item,['route' => ['cart.update', $item->id], 'method' => 'put', 'class' => 'form-columns', 'id' => 'modal-form' ]) !!}
                            {{ Form::hidden('product_id', $item->id , ['id' => 'product_id']) }}

                            <span class="item-price">{{ $item->getPriceWithConditions() }} {{ $item->attributes['currency_attr'] }} X</span>
                            <input class="item-quantity" name="quantity" value="{{ $item->quantity }}" type="text" autocomplete="off">
                            <span class="item-price">{{ $item->attributes['unit_attr'] }} </span>

                            {!! Form::submit('Update', ['class' => "btn btn-primary update"]) !!}
                            {!! Form::close() !!}
                        </div>
                        <div class="col-md-4 item-price" style="text-align: right;padding-right: 50px;">
                            {{ $item->getPriceSumWithConditions() }} {{ $item->attributes['currency_attr'] }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
