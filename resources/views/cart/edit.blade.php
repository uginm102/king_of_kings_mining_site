@extends('layouts.cart-app')

@section('content')
    <div class="cart" >

        {!! Form::model($product,['route' => ['cart.update', $product->id], 'method' => 'put', 'class' => 'form-columns', 'id' => 'modal-form' ]) !!}
        {{ Form::hidden('product_id', $product->id , ['id' => 'product_id']) }}
        <div class="row cart_item">
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-danger {{ $errors->any() ? '' : 'invisible' }}">
                            <ul>
                                @foreach ($errors->all() as $message)
                                    <li>{{$message}}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <a href="#" class="cart_product_image">
                            <img src="{{ asset("images/product.png") }}" alt="" width="100" height="100"/>
                        </a>
                    </div>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-12 item-title"><a href="#">{{ $product->name }}</a></div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 item-code">Code: {{ $product->code }}</div>
                            <div class="col-md-12">
                                <span class="item-price">{{ $product->price }} X</span>
                                <input class="item-quantity" value="20" type="text" name="quantity" autocomplete="off">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5 cart_details">
                                {!! Form::submit('Update', ['class' => "btn btn-primary btn-checkout"]) !!}
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        {!! Form::close() !!}

    </div>
@endsection
