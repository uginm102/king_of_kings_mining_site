@extends('layouts.cart-app')
@section('title', 'Add to cart')
@section('content')
    <div class="cart">

        {!! Form::model($product,['route' => ['cart.store'], 'method' => 'post', 'class' => 'form-columns', 'id' => 'modal-form' ]) !!}
        {{ Form::hidden('product_id', $product->id , ['id' => 'product_id']) }}
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-danger {{ $errors->any() ? '' : 'invisible' }}">
                            <ul>
                                @foreach ($errors->all() as $message)
                                    <li>{{$message}}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-sm-12">
                        <a href="#" class="cart_product_image">
                            <img src="{{ asset("images/product.png") }}" alt="" width="150" height="150"/>
                        </a>
                    </div>
                    <div class="col-md-8 col-sm-12">
                        <div class="row cart_details">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-12 item-title"><a href="#">{{ $product->name }}</a></div>
                                </div>
                                <div class="row" style="margin-bottom: 20px;">
                                    <div class="col-md-12 item-code">Code: <strong>{{ $product->code }}</strong></div>
                                    <div class="col-md-12">
                                        <span class="item-price">{{ $product->priceDisp() }} {{ $product->currency }} X</span>
                                        <input class="item-quantity" value="20" type="text" name="quantity" autocomplete="off">
                                        <span>{{ $product->unit }} </span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 col-sm-10 offset-sm-2 offset-md-0">
                                        <button class="btn btn-primary btn-checkout" type="submit">
                                            <i class="fas fa-shopping-cart"></i> &nbsp;Add to cart
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
        {!! Form::close() !!}

    </div>
@endsection
