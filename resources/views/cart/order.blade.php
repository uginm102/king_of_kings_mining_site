@extends('layouts.cart-app')
@section('title', 'Order')
@section('content')
    <div class="checkout" >
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Order #: {{ $order->code }}</div>

                    <div class="card-body">
                        <div class="row" style="margin-bottom: 10px">
                            <div class="col-sm-4">
                                <h6 class="mb-3">Details:</h6>
                                <div>Order<strong>#: {{ $order->code }}</strong></div>
                                <div>{{ $order->date->toFormattedDateString() }}</div>
                                <div>Name: {{ $order->customer->full_name }}</div>
                                <div>E-mail: {{ $order->customer->email }}</div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive-sm">
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th style="width: 5%">#</th>
                                            <th style="width: 20%">Item</th>
                                            <th style="width: 15%">Code</th>
                                            <th style="width: 20%; text-align: right;">Quantity</th>
                                            <th style="width: 20%; text-align: right;">Unit Cost</th>
                                            <th style="width: 20%; text-align: right;">Total</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @forelse ($order->orderItems as $item)
                                            <tr>
                                                <td > {{ $loop->index + 1 }} </td>
                                                <td >{{ $item->name }}</td>
                                                <td >{{ $item->code }}</td>
                                                <td class="right">{{ $item->quantityDisp() }} {{ $item->unit }}</td>
                                                <td class="right">{{ $item->unitPriceDisp() }} {{ $order->currency}}</td>
                                                <td class="right">{{ $item->amountDisp() }} {{ $order->currency}}</td>
                                            </tr>
                                        @empty
                                            @include('shared.empty',['message' => 'Order has no items.'])
                                        @endforelse
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5">
                                NB: You will be redirected to an external payment gateway.
                            </div>
                            <div class="col-md-5 ml-auto">
                                <table class="table table-clear">
                                    <tbody>
                                    <tr>
                                        <td class="left"><strong>Total</strong></td>
                                        <td class="right"><strong>{{ $order->amountDisp() }} {{ $order->currency}}</strong></td>
                                    </tr>
                                    </tbody>
                                </table>
                                {!! Form::open(['route' => ['cart.pay'], 'method' => 'post', 'target' => '_blank',
                                    'class' => 'form-columns', 'id' => 'modal-form' ]) !!}
                                {{ Form::hidden('order_id', $order->id , ['id' => 'order_id']) }}
                                <button type="submit" class="btn btn-primary btn-checkout" id="order_btn">
                                    <i class="fas fa-shopping-cart"></i> &nbsp;
                                    Pay
                                </button>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
