<div class="row {{ $loop->last ? 'last' : '' }}">
    <div class="col-md-12">
        <div class="cart_item">
            <div class="row">
                <div class="col-md-2">
                    <a href="#" class="cart_product_image">
                        <img src="{{ asset("images/product.png") }}" alt="" width="100" height="100"/>
                    </a>
                </div>
                <div class="col-md-10">
                    <div class="row">
                        <div class="col-md-11 item-title"><a href="#">{{ $item->name }}</a></div>
                    </div>

                    <div class="row" style="padding-top: 10px;">
                        <div class="col-md-4 item-code">Code: {{ $item->attributes['code_attr'] }}</div>
                        <div class="col-md-4">
                            <span class="item-price">{{ $item->getPriceWithConditions() }} {{ $item->attributes['currency_attr'] }} X {{ $item->quantity }} {{ $item->attributes['unit_attr'] }}</span>
                        </div>
                        <div class="col-md-4 item-price" style="text-align: right;padding-right: 50px;">
                            {{ $item->getPriceSumWithConditions() }} {{ $item->attributes['currency_attr'] }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
