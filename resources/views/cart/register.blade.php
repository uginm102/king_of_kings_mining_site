@extends('layouts.cart-app')
@section('title', 'Checkout')
@section('content')
    <div class="checkout" >
        <div class="row">
            <div class="offset-2 col-md-8">
                <div class="alert alert-danger col-md-12 {{ $errors->any() ? '' : 'invisible' }} " role="alert">
                    @foreach ($errors->all() as $message)
                        <div>{{$message}}</div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 offset-2">
                <div class="card">
                    <div class="card-header">Register</div>

                    <div class="card-body">
                        {!! Form::open(['route' => ['cart.register'], 'method' => 'post', 'class' => 'form-columns', 'id' => 'modal-form' ]) !!}

                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label">First Name</label>
                                <div class="col-md-12">
                                    <input id="first_name" type="text" class="form-control" name="first_name" value="" required="" autofocus="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label">Last Name</label>
                                <div class="col-md-12">
                                    <input id="last_name" type="text" class="form-control" name="last_name" value="" required="" autofocus="">
                                </div>
                            </div>
                        <div class="form-group">
                            <label for="name" class="col-md-12 control-label">Telephone</label>
                            <div class="col-md-12">
                                <input id="telephone" type="text" class="form-control" name="telephone" value="" required="" autofocus="">
                            </div>
                        </div>
                            <div class="form-group">
                                <label for="email" class="col-md-12 control-label">E-Mail Address</label>
                                <div class="col-md-12">
                                    <input id="email" type="email" class="form-control" name="email" value="" required="">
                                </div>
                            </div>
                        <div class="form-group">
                            <label for="name" class="col-md-12 control-label">Address</label>
                            <div class="col-md-12">
                                <textarea id="address" class="form-control" name="address" required="" autofocus=""></textarea>
                            </div>
                        </div>
                            <div class="form-group">
                                <label for="password" class="col-md-12 control-label">Password</label>
                                <div class="col-md-12">
                                    <input id="password" type="password" class="form-control" name="password" required="">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="password-confirm" class="col-md-12 control-label">Confirm Password</label>
                                <div class="col-md-12">
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required="">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-12 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Register
                                    </button>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">Login</div>

                    <div class="card-body">
                        {!! Form::open(['route' => ['cart.authenticate'], 'method' => 'post', 'class' => 'form-columns', 'id' => 'modal-form' ]) !!}

                            <div class="form-group">
                                <label for="email" class="col-md-12 control-label">E-Mail Address</label>

                                <div class="col-md-12">
                                    <input id="email" type="email" class="form-control" name="email" value="" required="" autofocus="">

                                </div>
                            </div>

                            <div class="form-group">
                                <label for="password" class="col-md-12 control-label">Password</label>

                                <div class="col-md-12">
                                    <input id="password" type="password" class="form-control" name="password" required="">

                                </div>
                            </div>

{{--                            <div class="form-group">--}}
{{--                                <div class="col-md-12 col-md-offset-4">--}}
{{--                                    <div class="checkbox">--}}
{{--                                        <label>--}}
{{--                                            <input type="checkbox" name="remember"> Remember Me--}}
{{--                                        </label>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}

                            <div class="form-group">
                                <div class="col-md-12 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Login
                                    </button>

{{--                                    <a class="btn btn-link" href="#">--}}
{{--                                        Forgot Your Password?--}}
{{--                                    </a>--}}
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
