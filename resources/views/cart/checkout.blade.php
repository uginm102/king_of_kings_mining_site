@extends('layouts.cart-app')
@section('title', 'Cart')
@section('content')
    <div class="cart">
        <div class="row" >
            <div class="col-md-8 col-sm-12">
                <div class="row">
                    <div class="col-md-12">

                    </div>
                </div>
                @forelse ($items as $item)
                    @include('cart.checkout-item', ['item' => $item])
                @empty
                    @include('shared.empty',['message' => 'Cart empty.'])
                @endforelse
            </div>
            <div class="col-md-4 col-sm-12">
                <div class="row cart_details">
                    <div class="col-md-12 ">
                        <div class="row" style="margin-bottom: 20px;">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-4 cart_label">Name:</div>
                                    <div class="col-md-8 ">
                                        {{ $user->full_name }}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 cart_label">Email:</div>
                                    <div class="col-md-8 ">
                                        {{ $user->email }}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-4 cart_label">Subtotal:</div>
                                    <div class="col-md-8 cart_amount">{{ $cart->getTotal() }}</div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-4">Shipping:</div>
                                    <div class="col-md-8 cart_amount">0</div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-4 cart_total_label">Total:</div>
                                    <div class="col-md-8 cart_amount">{{ $cart->getTotal() }}</div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                {!! Form::open(['route' => ['cart.makeOrder'], 'method' => 'post', 'class' => 'form-columns', 'id' => 'modal-form' ]) !!}
                                <button type="submit" class="btn btn-primary btn-checkout" id="order_btn">
                                    Make order
                                </button>
                                {!! Form::close() !!}
                            </div>
                            <div class="col-md-12" style="margin-top: 5px;">
                                <a class="continue-shopping" href="{{ route('home') }}">Continue shopping</a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>


@endsection
@section('include-js')

@endsection
