<!-- Site Title-->
<title>{{ config('app.name', '') }} - @yield('title')</title>
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="format-detection" content="telephone=no">
<meta name="viewport"
      content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="utf-8">
<link rel="icon" href="images/favicon.ico" type="image/x-icon">
<!-- Stylesheets-->
<link rel="stylesheet" type="text/css"
      href="//fonts.googleapis.com/css?family=Lato:300,300italic%7CMontserrat:400,700">
<link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet" type="text/css">
<link href="{{ mix('css/compiled/app.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('css/fonts.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('css/toastr.min.css') }}" rel="stylesheet" type="text/css">
<style>.ie-panel {
        display: none;
        background: #212121;
        padding: 10px 0;
        box-shadow: 3px 3px 5px 0 rgba(0, 0, 0, .3);
        clear: both;
        text-align: center;
        position: relative;
        z-index: 1;
    }

    html.ie-10 .ie-panel, html.lt-ie-10 .ie-panel {
        display: block;
    }
</style>
