<header class="page-header {{ $loc }}">
    <!-- RD Navbar-->
    <div class="rd-navbar-wrap">
        <nav class="rd-navbar {{ $loc == 'home' ? 'rd-navbar_transparent' : '' }} rd-navbar_boxed"
             data-layout="rd-navbar-fixed"
             data-sm-layout="rd-navbar-fixed" data-md-layout="rd-navbar-fixed" data-md-device-layout="rd-navbar-fixed"
             data-lg-layout="rd-navbar-static" data-lg-device-layout="rd-navbar-fixed" data-xl-layout="rd-navbar-static"
             data-xl-device-layout="rd-navbar-static" data-xxl-layout="rd-navbar-static"
             data-xxl-device-layout="rd-navbar-static" data-lg-stick-up="true" data-xl-stick-up="true"
             data-xxl-stick-up="true" data-lg-stick-up-offset="50px" data-xl-stick-up-offset="50px"
             data-xxl-stick-up-offset="50px" data-body-class="rd-navbar-absolute">
            <!-- RD Navbar Top Panel-->
            <div class="rd-navbar-top-panel">
                <div class="rd-navbar-top-panel__main">
                    <div class="rd-navbar-top-panel__toggle rd-navbar-fixed__element-1 rd-navbar-static--hidden"
                         data-rd-navbar-toggle=".rd-navbar-top-panel__main"><span></span></div>
                    <div class="rd-navbar-top-panel__content">
                        <div class="rd-navbar-top-panel__left">
                            <ul class="rd-navbar-items-list">
                                <li>
                                    <div class="unit flex-row align-items-center unit-spacing-xs">
                                        <div class="unit__left"><span
                                                class="icon novi-icon icon-sm icon-primary linearicons-map-marker"></span>
                                        </div>
                                        <div class="unit__body">
                                            <p><a href="#">Address: Plot 12 Luthuri Avenue. P.O.Box 2000 Kampala.</a>
                                            </p>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="unit flex-row align-items-center unit-spacing-xs">
                                        <div class="unit__left"><span
                                                class="icon novi-icon icon-sm icon-primary linearicons-telephone"></span>
                                        </div>
                                        <div class="unit__body">
                                            <ul class="list-semicolon">
                                                <li><a href="tel:#">(+256) 772-722-132</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="rd-navbar-top-panel__right">
                            <ul class="list-inline-xxs">
                                @auth
                                    <li>
                                        {{-- TODO add user management --}}
                                        <a class="icon novi-icon icon-xxs icon-gray-darker" href="#">
                                            {{ auth()->user()->full_name }}
                                        </a>
                                    </li>
                                    @can('admin')
                                        <li><a class="icon-gray-darker" href="{{ route('admin.orders') }}">Orders</a></li>
                                    @endcan
                                @endauth
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="rd-navbar-inner rd-navbar-search-wrap">
                <!-- RD Navbar Panel-->
                <div class="rd-navbar-panel rd-navbar-search-lg_collapsable">
                    <button class="rd-navbar-toggle" data-rd-navbar-toggle=".rd-navbar-nav-wrap"><span></span></button>
                    <!-- RD Navbar Brand-->
                    <div class="rd-navbar-brand">
                        <a class="brand-name" href="{{ route('home') }}">
                            <img src="{{ asset('images/logo.png') }}" alt="" width="304" height="39"/>
                        </a>
                    </div>
                </div>
                <!-- RD Navbar Nav-->
                <div class="rd-navbar-nav-wrap rd-navbar-search_not-collapsable">
                    <div class="rd-navbar-search_collapsable">
                        <ul class="rd-navbar-nav">
                            @if($loc == 'home')
                                <li><a href="#home">Home</a></li>
                                <li><a href="#about">About</a></li>
                                <li><a href="#products">Products</a></li>
                                <li><a href="#contacts">Contacts</a></li>
                            @else
                                <li><a href="{{ route('home') }}">Home</a></li>
                            @endif

                            @auth
                                <li><a href="{{ route('cart.orders') }}">My orders</a></li>
                                <li><a href="{{ route('logout') }}">Logout</a></li>
                            @endauth
                            @guest
                                <li><a href="{{ route('login') }}">Login</a></li>
                            @endguest
                            <li class="shopping_cart">
                                <a href="{{ route('cart.index') }}" class="shopping_cart_icon">
                                    <i class="fas fa-shopping-cart fa-2x">
                                            <span class="count">
                                                @if(session()->has('cart_count'))
                                                    {{session('cart_count')}}
                                                @else
                                                    0
                                                @endif
                                            </span>
                                    </i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
    </div>
</header>
