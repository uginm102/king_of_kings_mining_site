<!DOCTYPE html>
<html class="wide wow-animation" lang="en">
<head>
    @include('layouts.head')
</head>
<body>
<div class="ie-panel">
    <a href="http://windows.microsoft.com/en-US/internet-explorer/">
        <img src="{{ asset("images/ie8-panel/warning_bar_0000_us.jpg") }}" height="42" width="820"
             alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today.">
    </a>
</div>
<div class="preloader">
    <div class="cssload-container">
        <div class="cssload-speeding-wheel"></div>
    </div>
</div>
<!-- Page-->
<div class="page">
    @include('layouts.header', ['loc' => 'cart'])
    @yield('content')
    @include('layouts.footer')
</div>
<!-- Javascript-->
<script src="{{ asset('js/core.min.js') }}"></script>
<script src="{{ asset('js/script.js') }}"></script>
<script src="{{ asset('js/toastr.min.js') }}"></script>
<script type="text/javascript">
    $(function () {

        @if (session('info'))
        toastr.info('{{session('info')}}');
        @endif

        @if (session('warning'))
        toastr.warning('{{session('warning')}}');
        @endif

        @if (session('success'))
        toastr.success('{{session('success')}}');
        @endif

        @if (session('error'))
        toastr.error('{{session('error')}}');
        @endif
    });
</script>
@yield('js')
<!-- coded by barber-->
</body>
</html>
