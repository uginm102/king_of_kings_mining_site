<!DOCTYPE html>
<html lang="en">
<head>
    <title>{{ config('app.name', '') }} - @yield('title')</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
    <!--===============================================================================================-->

    <link rel="stylesheet" type="text/css" href="{{ asset('css/fonts.css') }}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/icon-font.min.css') }}">
    <!--===============================================================================================-->
    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/auth/util.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/auth/main.css') }}">
    <!--===============================================================================================-->
</head>
<body>

<div class="limiter">
    <div class="container-login100">
        @yield('content')
    </div>
</div>

</body>
</html>
