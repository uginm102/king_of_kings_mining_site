<div class="alert alert-info center">
    <span><i class="icon-warning-sign"></i></span>
    <span><em>{{ $message }}</em></span>
</div>