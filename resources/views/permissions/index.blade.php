@extends('layouts.app')
@section('title', 'Permission')

@section('head-css')
    <!-- this page specific styles -->
    <link href="{{ asset('css/compiled/list.css') }}" rel="stylesheet" media="screen">
    <link href="{{ mix('css/datatables.css') }}" rel="stylesheet" media="screen">
@endsection

@section('content')


<div class="row">
        <div class="col-xl-9 col-lg-12 nopadding" >
            <div class="card-box rowouter">
             

<div class="row rowinner">
<div class="col-xl-12">   

<div class="card-header bg-dark text-white">
<div class="card-widgets text-light">
        <a href="{{route('admin.permissions.create')}}" data-ajax=true
        class="actionbtn">
         <span>&#43;</span>
         New Permission
     </a>
</div>
<h5 class="card-title mb-0 text-light">Permissions</h5>
</div>


<div class="card-box">
<div class="row">
  <!-- Users table -->

        <div class="col-md-12">
                <div class="col-md-12">
                        <div id="reload-container-permissions">
                            @include('admin.permissions.list')
                        </div>
                    </div>
        </div>


</div></div>

</div></div></div></div></div>





<div class="clearfix"></div>
   
@endsection
@section('include-js')
    <script src="{{ asset('js/moment.js') }}"></script>


    <script src="{{ mix('js/datatables.js') }} }}"></script>

<script src="{{ asset('assets/js/app.min.js') }}"></script>
@endsection

@section('js')
    <script type="text/javascript">
        $(function () {
            $('.data-table').dataTable({
                "sPaginationType": "full_numbers",
                'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': ['nosort']
                }]
            });

            main.initAjax();
        });
    </script>
@endsection