@extends('layouts.modal-app')
@section('modal-content')
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title">Delete a permission</h4>
        </div>
        {!! Form::model($permission, ['route' => ['admin.permissions.destroy', $permission],
    'method' => 'DELETE', 'class' => 'form-columns', 'id' => 'modal-form' ]) !!}
    {{ csrf_field() }}
        <div class="modal-body">
            {{--{{session()->get('e') }}--}}
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-danger {{ $errors->any() ? '' : 'hidden' }}">
                        <ul>
                            @foreach ($errors->all() as $message)
                                <li>{{$message}}</li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                        {!!  Form::label('name', 'Name', ['class' => '']) !!}
                        <p class="form-control-static">
                            {{ $permission->name }}
                        </p>
                    </div>
                </div>

            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            {!! Form::submit(__('general.submit'), ['class' => "btn btn-primary pull-right"]) !!}
        </div>
        {!! Form::close() !!}
    </div><!-- /.modal-content -->
@endsection

@section('include-js')
@endsection
@section('js')
    <script type="text/javascript">
        $(function () {

        });
    </script>
@endsection