@if(!$permissions->isEmpty())
    <table class="table table-hover data-table">
        <thead>
        <tr>
            <th class="col-md-10 sortable">
                Name
            </th>
        </tr>
        </thead>
        <tfoot>
        <th class="col-md-12 sortable">
            Name
        </th>
        </tfoot>
        <tbody>
        @foreach ($permissions as $permission)
            <!-- row -->
            <tr class="first">
                <td>
                        <span class="jobtitle"> {{ $permission->name }}</span>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@else
    @include('shared.empty',['message' => 'No permissions found.'])
@endif
<script type="text/javascript">
    $(function () {
        main.initAjax();
    });
</script>