@extends('layouts.app')
@section('title', 'Permission')

@section('head-css')
    <!-- this page specific styles -->
    <link href="{{ asset('css/compiled/detail-page.css') }}" rel="stylesheet" media="screen">
@endsection

@section('content')

    <div id="pad-wrapper" class="detail">
        <!-- header -->
        <div class="row header">
            <div class="col-md-8">
                <img src="{{ asset('img/smartcase/puzzle.png') }}" class="avatar img-circle" alt="contact"/>
                <h3 class="name">{{$permission->name}}</h3>
            </div>
            <a class="btn-flat icon pull-right delete" data-toggle="tooltip" title="Delete user"
               data-placement="top">
                <i class="icon-trash"></i>
            </a>
            <a class="btn-flat icon large pull-right edit">
                Edit permission
            </a>
        </div>

        <div class="row profile">
            <!-- bio, new note & orders column -->
            <div class="col-md-9 bio">
                <div class="profile-box">
                    <div class="row">
                        <div class="col-md-12 section">
                            {{--<h6>Roles</h6>--}}
                            <div class="pull-right action-button">
                                {{--<a href="#">--}}
                                    {{--<i class="fas fa-plus"></i>--}}
                                    {{--Add--}}
                                {{--</a>--}}
                            </div>
                            <!-- recent orders table -->
                            {{--<table class="table table-hover">--}}
                                {{--<thead>--}}
                                {{--<tr>--}}
                                    {{--<th class="col-md-9">--}}
                                        {{--Name--}}
                                    {{--</th>--}}
                                    {{--<th class="col-md-3">--}}
                                        {{--<span class="line"></span>--}}
                                    {{--</th>--}}
                                {{--</tr>--}}
                                {{--</thead>--}}
                                {{--<tbody>--}}
                                {{--<!-- row -->--}}
                                {{--<tr class="first">--}}
                                    {{--<td>--}}
                                        {{--<a href="#">#459</a>--addDays(29) --}}
                                        {{--Test--}}
                                    {{--</td>--}}
                                    {{--<td>--}}
                                        {{--Transport--}}
                                    {{--</td>--}}
                                {{--</tr>--}}
                                {{--</tbody>--}}
                            {{--</table>--}}
                        </div>
                    </div>
                </div>
            </div>

            <!-- side address column -->
            <div class="col-md-3 col-xs-12 address pull-right">
            </div>
        </div>
    </div>
@endsection