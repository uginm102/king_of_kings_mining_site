@if( $permission->id > 0 )
    {!! Form::model($permission, ['route' => ['admin.permissions.update', $role],
    'method' => 'put', 'class' => 'form-columns' ]) !!}
@else
    {!! Form::open(['route' => 'admin.permissions.store', 'method' => 'post', 'class' => 'form-columns']) !!}
@endif
{{ csrf_field() }}
{{--{{session()->get('e') }}--}}
<div class="row">
    <div class="col-md-12">
        <div class="alert alert-danger {{ $errors->any() ? '' : 'hidden' }}">
            <ul>
                @foreach ($errors->all() as $message)
                    <li>{{$message}}</li>
                @endforeach
            </ul>
        </div>
        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
            {!!  Form::label('name', 'Name', ['class' => '']) !!}
            {!! Form::text('name',$permission->name,
            ['class' => "form-control", 'placeholder' => 'name']) !!}
        </div>
    </div>

    <div class="col-md-12">
        <div class="form-group clearfix">
            {!! Form::submit(__('general.submit'), ['class' => "btn btn-primary pull-right"]) !!}
            @if( $permission->id > 0 )
                <a href="{{ route('admin.permissions.show', $permission) }}"
                   class="btn btn-link pull-right">{{ __('general.cancel') }}</a>
            @else
                <a href="{{ route('admin.permissions.index') }}"
                   class="btn btn-link pull-right">{{ __('general.cancel') }}</a>
            @endif
        </div>
    </div>
</div>
{!! Form::close() !!}