<?php

namespace App\Providers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\View;
use ReflectionClass;
use Illuminate\Support\Facades\File;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->bootstrapInterfaces( '');
    }

    public function bootstrapInterfaces( $folderName)
    {
        foreach (File::files(app_path("Services")) as $file) {
            $fileName = $file->getBasename('.php');
            $oReflectionClass = new ReflectionClass('App\Services\\'.  $fileName);
            if (!$oReflectionClass->isInterface() && $oReflectionClass->name != 'App\Services\VPCPaymentConnection') {
                //dd($oReflectionClass->getInterfaceNames()[0]);
                try{
                    $interfaceName = $oReflectionClass->getInterfaceNames()[0];

                    $this->app->bind(
                        "{$interfaceName}",
                        "{$oReflectionClass->name}"
                    );
                }
                catch (\Exception $ex)
                {
                    //dd($oReflectionClass->name);
                    //dd($ex);
                }

            }
        }
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //https://laravel.com/docs/7.x/views#passing-data-to-views
        //Todo seems to run before authentication.
        //$currentUser = Auth::user();
        //dd($currentUser);
        //View::share('currentUser', $currentUser);

        //https://medium.com/@marcel_domke/laravel-view-composer-dba46a17456e
        app('view')->composer('layouts.header', function ($view) {
            $action = app('request')->route()->getAction();

            //$currentUser = Auth::user();

            $prefix = request()->route()->getPrefix();

            $prefix = Str::replaceLast('/', '', $prefix);

            $fullController = class_basename($action['controller']);

            list($controller, $action) = explode('@', $fullController);

            $view->with(compact('controller', 'action', 'prefix', 'fullController'));
        });
    }
}
