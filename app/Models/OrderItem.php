<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string name
 * @property string code
 * @property string unit
 * @property float quantity
 * @property float unit_price
 * @property float amount
 * @property int order_id
 * @property User createdBy
 * @property int created_by
 */
class OrderItem extends Model
{
    public function quantityDisp()
    {
        return number_format($this->quantity, 2);
    }

    public function unitPriceDisp()
    {
        return number_format($this->unit_price, 2);
    }

    public function amountDisp()
    {
        return number_format($this->amount, 2);
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by');
    }
}
