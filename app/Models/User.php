<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Exception;


/**
 * @property string password
 * @property string email
 * @property boolean is_active
 * @property int id
 * @property mixed passwordSecurities
 * @property mixed|string first_name
 * @property mixed|string last_name
 * @property mixed|string telephone
 * @property mixed|string address
 */
class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'password','email',
    ];

    public function getFullNameAttribute()
    {
        $fullName = $this->middle_name != '' ?
            "{$this->first_name} {$this->middle_name} {$this->last_name}"
            : "{$this->first_name} {$this->last_name}";

        if ($this->salutation) {
            $salutationCode = $this->salutation->code;
            $fullName = "$salutationCode $fullName";
        }

        return $fullName;
    }

    public function getNameAttribute()
    {
        return $this->email;
    }
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }
}
