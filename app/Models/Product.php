<?php


namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * @property string name
 * @property string code
 * @property string currency
 * @property string unit
 * @property float price
 * @property bool is_active
 * @property string image
 * @property User createdBy
 * @property int created_by
 */
class Product extends Model
{
    public function priceDisp()
    {
        return number_format($this->price, 2);
    }
}
