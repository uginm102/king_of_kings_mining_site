<?php

namespace App\Http\Requests;

use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;

/**
 * @property string first_name
 * @property string last_name
 * @property string middle_name
 * @property string telephone
 * @property string email
 * @property string password
 * @property string password_confirmation
 * @property int created_by
 * @property int updated_by
 * @property string address
 */
class RegisterUserPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|string|max:150',
            'last_name' => 'required|string|max:150',
            'middle_name' => 'nullable|string|max:150',
            'telephone' => 'nullable|string|max:150',
            'address' => 'nullable|string',
            'email' => 'required|email|max:150|unique:users',
            'password' => ['required','string','confirmed'],
        ];
    }
}
