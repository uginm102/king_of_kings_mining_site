<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use Illuminate\View\View;
use App\Http\Controllers\Controller;
use App\Http\Requests\AssignPermissionsPost;
use App\Http\Requests\AssignRolesPost;
use App\Http\Requests\UserSearchPost;
use App\Services\iUserService;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Cache;

class UsersController extends Controller
{
    protected $userService;

    public function __construct(iUserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param UserSearchPost $request
     * @return Factory|Response|View
     */
    public function index(UserSearchPost $request)
    {
        $users = $this->userService->list($request);

        return view('users.index', ['users' => $users, 'q' => $request->q,
            'username' => $request->username]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Factory|RedirectResponse|Response|View
     */
    public function show($id)
    {
        $user = $this->userService->get($id);
        $locked = Cache::has($user->email . '|throttleKey');
        if (!$user) {
            parent::errorAlert("User not found");
            return redirect()->route('users.index');
        }

        return view('users.show', ['user' => $user, 'locked' => $locked]);
    }


    public function roles(int $id)
    {
        $user = $this->userService->get($id);
        return view('users.roles', ['user' => $user]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param int $id
     * @return Factory|Response|View
     */
    public function assignRole(int $id)
    {
        $user = $this->userService->get($id);
        $roles = Role::all();
        return view('users.assign-role-modal-form', ['user' => $user, 'roles' => $roles]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AssignRolesPost $request
     * @param int $id
     * @return JsonResponse|Response
     */
    public function assignRoleSubmit(AssignRolesPost $request, int $id)
    {
        try {
            $user = $this->userService->assignRole($request, $id);
            return response()->json(
                [
                    'message' => __('general.success'),
                    'url' => route('users.roles', $user),
                    'target' => '#reload-container-roles'
                ], 200);

        } catch (Exception $e) {
            Log::debug($e);
            // return some other error, or rethrow as above.
            return response()->json(
                [
                    'message' => $e->getMessage(),
                    'url' => route('roles.permissions', $id),
                    'text' => 'client->name'
                ], 400);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param int $id
     * @param int $role_id
     * @return Factory|Response|View
     * @internal param int $permission_id
     */
    public function removeRole(int $id, int $role_id)
    {
        $user = $this->userService->get($id);
        $role = Role::find($role_id);
        return view('users.remove-role-modal-form', ['user' => $user, 'role' => $role]);
    }

    public function removeRoleSubmit(Request $request, int $id)
    {
        try {
            $user = $this->userService->removeRole($request, $id);

            parent::successAlert(__('general.success'));

            return response()->json(
                [
                    'message' => __('general.success'),
                    'url' => route('users.roles', $user),
                    'target' => '#reload-container-roles'
                ], 200);

        } catch (Exception $e) {
            Log::debug($e);
            return response()->json(
                [
                    'message' => $e->getMessage(),
                    'text' => 'client->name'
                ], 400);
        }
    }


    /**
     * @param int $id
     * @return Factory|View
     */
    public function permissions(int $id)
    {
        $user = $this->userService->get($id);
        return view('users.permissions', ['user' => $user]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param int $id
     * @return Factory|Response|View
     */
    public function assignPermission(int $id)
    {
        $user = $this->userService->get($id);
        $permissions = Permission::all();
        return view('users.assign-permission-modal-form',
            ['user' => $user, 'permissions' => $permissions]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AssignPermissionsPost $request
     * @param int $id
     * @return JsonResponse|Response
     */
    public function assignPermissionSubmit(AssignPermissionsPost $request, int $id)
    {
        try {
            $user = $this->userService->assignPermission($request, $id);
            return response()->json(
                [
                    'message' => __('general.success'),
                    'url' => route('users.permissions', $user),
                    'target' => '#reload-container-permissions'
                ], 200);

        } catch (Exception $e) {
            Log::debug($e);
            // return some other error, or rethrow as above.
            return response()->json(
                [
                    'message' => $e->getMessage(),
                    'url' => route('roles.permissions', $id),
                    'text' => 'client->name'
                ], 400);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param int $id
     * @param int $permission_id
     * @return Factory|Response|View
     * @internal param int $permission_id
     */
    public function removePermission(int $id, int $permission_id)
    {
        $user = $this->userService->get($id);
        $permission = Permission::find($permission_id);
        return view('users.remove-permission-modal-form', ['user' => $user, 'permission' => $permission]);
    }

    public function removePermissionSubmit(Request $request, int $id)
    {
        try {
            $user = $this->userService->removePermission($request, $id);

            return response()->json(
                [
                    'message' => __('general.success'),
                    'url' => route('users.permissions', $user),
                    'target' => '#reload-container-permissions'
                ], 200);

        } catch (Exception $e) {
            Log::debug($e);
            return response()->json(
                [
                    'message' => $e->getMessage(),
                    'text' => 'client->name'
                ], 400);
        }
    }
    public function unlock(int $id)
    {
        $user = $this->userService->get($id);
        return view('users.unlock-modal-form', ['user' => $user]);
    }

    public function unlockSubmit(Request $request, int $id)
    {
        try {
            $user = $this->userService->get($id);
            Cache::forget($user->email . '|throttleKey');

            return response()->json(
                [
                    'action' => 'reload-page'
                ], 200);

        } catch (Exception $e) {
            Log::debug($e);
            return response()->json(
                [
                    'message' => $e->getMessage(),
                    'text' => __('general.action_error')
                ], 400);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Factory|RedirectResponse|Response|View
     */
    public function edit($id)
    {
        $user = $this->userService->get($id);

        if (!$user) {
            parent::errorAlert("User not found.");
            return redirect()->route('users.index');
        }

        return view('users.edit', ['user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return RedirectResponse|Response
     */
    public function update(Request $request, $id)
    {
        try {
            $user = $this->userService->updateEmail($request->email, $id);
            $user = $this->userService->updateAction($request->is_active, $id);

            parent::successAlert(__('general.success'));
            return redirect()->route('users.show', $user);
        } catch (Exception $e) {
            // return some other error, or rethrow as above.
            return back()->withErrors(['e' => $e->getMessage()])->withInput();
        }
    }


    /**
     * Show the form for creating a new resource.
     *
     * @param int $id
     * @return Factory|Response|View
     */
    public function resetPassword(int $id)
    {
        $user = $this->userService->get($id);
        return view('users.reset-password-modal-form', ['user' => $user]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param UserPost|Request $request
     * @param int $id
     * @return JsonResponse|Response
     */
    public function resetPasswordSubmit(Request $request, int $id)
    {
        try {
            $user = $this->userService->resetPassword($id);
            return response()->json(
                [
                    'message' => __('general.success'),
                ], 200);

        } catch (Exception $e) {
            Log::debug($e);
            // return some other error, or rethrow as above.
            return response()->json(
                [
                    'message' => $e->getMessage(),
                ], 400);
        }

    }
}
