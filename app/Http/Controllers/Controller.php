<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Log;

class Controller extends BaseController
{
    use DispatchesJobs, ValidatesRequests;

    use AuthorizesRequests {
        resourceAbilityMap as protected resourceAbilityMapTrait;
    }

    protected function infoAlert(string $message)
    {
        request()->session()->flash('info', $message);
    }

    protected function warningAlert(string $message)
    {
        request()->session()->flash('warning', $message);
    }

    protected function errorAlert(string $message)
    {
        request()->session()->flash('error', $message);
    }

    protected function successAlert(string $message)
    {
        request()->session()->flash('success', $message);
    }

    protected function debug($obj)
    {
        Log::debug($obj);
    }

    /**
     * Get the map of resource methods to ability names.
     *
     * @return array
     */
    protected function resourceAbilityMap()
    {
        // Remove show since we aren't loading models from the url
        // Remove edit since we aren't loading models from the url
        // Remove update since we aren't loading models from the url
        //unset($this->resourceAbilityMapTrait()['show']);
        // Map the "index" ability to the "index" function in our policies
        //return array_merge($this->resourceAbilityMapTrait(), ['index' => 'index']);

        return [
            //'show' => 'view',
            'create' => 'create',
            'store' => 'create',
            //'edit' => 'update',
            //'update' => 'update',
            'destroy' => 'delete',
            'index' => 'index'
        ];
    }
}
