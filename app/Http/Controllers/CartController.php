<?php

namespace App\Http\Controllers;

use App\Http\Requests\CartPost;
use App\Http\Requests\MerchantConfirmOrderPost;
use App\Http\Requests\RegisterUserPost;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Product;
use App\Models\User;
use App\Services\VPCPaymentConnection;
use Carbon\Carbon;
use Darryldecode\Cart\Cart;
use DB;
use Exception;
use DateTime;
use DateTimeZone;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\View\View;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Application|Factory|View
     */
    public function index(Request $request)
    {

        $items = [];
        $cart = \Cart::session(1);

        if (session()->has('cart_key')) {
            $userId = session('cart_key');

            $cart = \Cart::session($userId);

            $cart->getContent()->each(function ($item) use (&$items) {
                $items[] = $item;
            });
        }

        return view('cart.index', ['items' => $items, 'cart' => $cart]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return Application|Factory|View
     */
    public function create(Request $request)
    {
        $product = Product::find($request->product_id);
        return view('cart.create', ['product' => $product]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CartPost $request
     * @return RedirectResponse
     */
    public function store(CartPost $request)
    {
        try {
            $product = Product::find($request->product_id);

            if (Auth::check()) {
                $userId = Auth::user()->email;
            } else {
                $userId = session()->has('cart_key') ? session('cart_key') : (string)Str::uuid();
            }
            session()->put('cart_key', $userId);

            $customAttributes = [
                'code_attr' => $product->code,
                'currency_attr' => $product->currency,
                'unit_attr' => $product->unit
            ];

            \Cart::session($userId)->add($product->id, $product->name, $product->price, $request->quantity, $customAttributes);
            session()->put('cart_count', \Cart::session($userId)->getContent()->count());

            parent::successAlert(__('general.success'));
            return redirect()->route('cart.index');
        } catch (Exception $e) {
            parent::debug($e->getMessage());
            $message = 'Unknown';
            switch ($e->getMessage()){
                case 'validation.min.numeric':
                    $message = 'Quantity has to be above zero';
                    break;
            }
            // return some other error, or rethrow as above.
            return back()->withErrors(['e' => $message])->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return RedirectResponse
     */
    public function show($id)
    {
        parent::errorAlert(__('general.action_error'));
        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return RedirectResponse
     */
    public function edit($id)
    {
        parent::errorAlert(__('general.action_error'));
        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CartPost $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(CartPost $request, $id)
    {
        if (Auth::check()) {
            $userId = Auth::user()->email;
        } else {
            $userId = session()->has('cart_key') ? session('cart_key') : (string)Str::uuid();
        }

        \Cart::session($userId)->update($id, array(
            'quantity' => array(
                'relative' => false,
                'value' => $request->quantity
            ),
        ));

        parent::successAlert(__('general.success'));
        return redirect()->route('cart.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return RedirectResponse
     */
    public function destroy($id)
    {
        if (Auth::check()) {
            $userId = Auth::user()->email;
        } else {
            $userId = session()->has('cart_key') ? session('cart_key') : (string)Str::uuid();
        }

        \Cart::session($userId)->remove($id);

        session()->put('cart_count', \Cart::session($userId)->getContent()->count());

        parent::successAlert(__('general.success'));
        return redirect()->route('cart.index');
    }

    public function checkout()
    {
        if (!session()->has('cart_key')) {
            parent::errorAlert(__('general.action_error'));
            return redirect()->back();
        }
        $userId = session('cart_key');
        $cart = \Cart::session($userId);

        if (Auth::check()) {
            $items = [];

            $cart->getContent()->each(function ($item) use (&$items) {
                $items[] = $item;
            });
            $user = Auth::user();
            return view('cart.checkout', ['cart' => $cart, 'items' => $items, 'user' => $user]);
        } else {
            return view('cart.register', ['cart' => $cart]);
        }
    }

    /**
     * Handle an authentication attempt.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return RedirectResponse
     */
    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');
        $attempt = Auth::attempt($credentials);

        if ($attempt) {
            // Authentication passed...
            return redirect()->route('cart.checkout');
        } else {
            return redirect()->back()
                ->withInput($request->only('email'))
                ->withErrors(['email' => trans('auth.failed')]);
        }
    }

    public function register(RegisterUserPost $request)
    {
        $user = new User();
        $user->email = $request->email;
        $user->password = $request->password;
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->telephone = $request->telephone;
        $user->address = $request->address;
        $user->save();

        $credentials = $request->only('email', 'password');
        $attempt = Auth::attempt($credentials);

        if ($attempt) {
            // Authentication passed...
            return redirect()->route('cart.checkout');
        }
    }

    public function makeOrder(Request $request)
    {
        if (!session()->has('cart_key')) {
            parent::errorAlert(__('general.action_error'));
            return redirect()->back();
        }

        $user = Auth::user();
        $userId = session('cart_key');
        $cart = \Cart::session($userId);

        $items = [];
        $cart->getContent()->each(function ($item) use (&$items) {
            $items[] = $item;
        });

        DB::beginTransaction();
        $order = new Order();
        $order->code = $this->nextNumber(now());
        $order->date = now();
        $order->currency = 'USD';
        $order->customer_id = $user->id;
        $order->amount = str_replace(',', '', $cart->getTotal());
        $order->created_by = Auth::id();
        $order->save();
        foreach ($items as $item) {
            $orderItem = new OrderItem();
            $orderItem->name = $item->name;
            $orderItem->code = $item->attributes['code_attr'];
            $orderItem->unit = $item->attributes['unit_attr'];
            $orderItem->quantity = str_replace(',', '', $item->quantity);
            $orderItem->unit_price = str_replace(',', '', $item->price);
            $orderItem->amount = str_replace(',', '', $item->getPriceSumWithConditions());
            $orderItem->created_by = Auth::id();
            $orderItem->order_id = $order->id;
            $orderItem->save();
        }
        try {

            DB::commit();

            $cart->clear();
            session()->put('cart_count', $cart->getContent()->count());
            session()->remove('cart_key');

            return redirect()->route('cart.order', $order);
        } catch (\Exception $e) {
            DB::rollback();
            $response = response()->json(
                [
                    'message' => __('general.action_error'),
                    'e' => $e->getMessage()
                ], 500);
        }

        return $response;
    }

    private function nextNumber(Carbon $date)
    {
        $dateStr = $date->format('ymd');
        $clientCode = 'O';

        $lastClient = Order::where('code', 'like', "$clientCode-$dateStr%")
            ->whereRaw('date(created_at) = ?', [$date->toDateString()])
            ->orderBy('created_at', 'desc')->orderBy('id', 'desc')->first();

        if ($lastClient) {
            $pieces = explode("-", $lastClient->code);
            $lastNumber = (int)$pieces[2];
            $paddedNumber = str_pad(++$lastNumber, 4, "0", STR_PAD_LEFT);
            return "$clientCode-{$dateStr}-{$paddedNumber}";
        } else {
            return "$clientCode-{$dateStr}-0001";
        }
    }

    public function pay(Request $request)
    {
        if (!$request->order_id) {
            parent::errorAlert("Please specify the order to pay for.");
            return redirect()->back();
        }
        $user = Auth::user();
        $order = Order::with(['orderItems', 'customer'])->findOrFail($request->order_id);

        if ($order->customer_id != $user->id) {
            parent::errorAlert("Invalid order.");
            return redirect()->back();
        }

        $items = [];
        $order->orderItems->each(function ($item) use (&$items) {
            $items[] = $item->name;
        });

        $conn = new VPCPaymentConnection();

        // This is secret for encoding the SHA256 hash
        // This secret will vary from merchant to merchant

        $secureSecret = env("MERCHANT_SECRET", null);

        // Set the Secure Hash Secret used by the VPC connection object
        $conn->setSecureSecret($secureSecret);


        // *******************************************
        // START OF MAIN PROGRAM
        // *******************************************
        // Sort the POST data - it's important to get the ordering right
        //ksort($_POST);

        // add the start of the vpcURL querystring parameters
        $vpcURL = 'https://ibank.gtbank.co.ug/GTBANK/ABGTPAY/GTPAY/GTPay_v2/GTPay.aspx';

        // Add VPC post data to the Digital Order
        $conn->addDigitalOrderField('gtp_Amount', $order->amount);
        $conn->addDigitalOrderField('gtp_Currency', env("CURRENCY", null));
        $conn->addDigitalOrderField('gtp_CustomerCode', env("MERCHANT_CUSTOMER_CODE", null));
        $conn->addDigitalOrderField('gtp_OrderId', $order->code);
        $conn->addDigitalOrderField('gtp_PayerName', $order->customer->full_name);

        $conn->addDigitalOrderField('gtp_TransDetails', collect($items)->implode(', '));

        $dt = new DateTime();
        $dt->setTimeZone(new DateTimeZone('UTC'));
        $dat = $dt->format('Y-m-d\TH-i-s\Z');

        $conn->SetSecretVal(env("MERCHANT_SECRET_VAL", null));

        // Obtain a one-way hash of the Digital Order data and add this to the Digital Order
        $secureHash = $conn->hashAllFields();

        $conn->addDigitalOrderField("gtp_TransDate", $dat);
        $conn->addDigitalOrderField("gtp_SecureHash", $secureHash);
        $conn->addDigitalOrderField("gtp_SecureHashType", env("MERCHANT_SECRET_VAL", null));


        // Obtain the redirection URL and redirect the web browser
        $vpcURL = $conn->getDigitalOrder($vpcURL);
        return redirect()->away($vpcURL);
    }

    public function order(int $id)
    {
        $user = Auth::user();
        $order = Order::with(['orderItems', 'customer'])->find($id);
        if (!$order) {
            parent::errorAlert(__('general.action_error'));
            return redirect()->route('cart.orders');
        }

        return view('cart.order', ['order' => $order, 'user' => $user]);
    }

    public function orders()
    {
        $user = Auth::user();
        $orders = Order::with(['orderItems', 'customer'])->where('customer_id', $user->id)->get();

        return view('cart.orders', ['orders' => $orders, 'user' => $user]);
    }

    public function merchantConfirmOrder(MerchantConfirmOrderPost $request)
    {
        try {
            $conn = new VPCPaymentConnection();

            // This is secret for encoding the SHA256 hash
            // This secret will vary from merchant to merchant
            $secureSecret = env("MERCHANT_SECRET", null);

            // Set the Secure Hash Secret used by the VPC connection object
            $conn->setSecureSecret($secureSecret);

            $conn->addDigitalOrderField('message', $request->message);
            $conn->addDigitalOrderField('transaction_id', $request->transaction_id);

            $conn->SetSecretVal(env("MERCHANT_SECRET_VAL", null));

            // Obtain a one-way hash of the Digital Order data and add this to the Digital Order
            $secureHash = $conn->hashAllFields();

            if ($secureHash == $request->secure_hash) {
                $order = Order::where('code', $request->transaction_id)->get();
                $order->is_cleared = 1;
                $order->save();
                return response()->json(
                    [
                        'transaction_id' => $request->transaction_id,
                        'message' => 1
                    ], 200);
            } else {
                parent::debug('Start ****************************************************');
                parent::debug($request);
                parent::debug('Hash not matching');
                parent::debug('End ****************************************************');
                return response()->json(
                    [
                        'transaction_id' => $request->transaction_id,
                        'message' => 0
                    ], 502);
            }

        } catch (\Exception $e) {
            parent::debug('Start ****************************************************');
            parent::debug($request);
            parent::debug($e);
            parent::debug('End ****************************************************');
            // return some other error, or rethrow as above.
            return response()->json(
                [
                    'transaction_id' => $request->transaction_id,
                    'message' => 0
                ], 500);
        }
    }
}
