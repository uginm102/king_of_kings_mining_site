<?php
/**
 * Created by PhpStorm.
 * User: eugene
 * Date: 14/03/2018
 * Time: 22:22
 */
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use App\Http\Requests\AssignPermissionsPost;
use App\Services\iRoleService;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Exception;
use Illuminate\Support\Facades\Log;

class RolesController extends Controller
{
    protected $roleService;

    public function __construct(iRoleService $roleService)
    {
        $this->roleService = $roleService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $roles = Role::all();
        return view('roles.index', ['roles' => $roles]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $role = new Role();
        return view('roles.create', ['role' => $role]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param UserPost|Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            $role = Role::create(['name' => $request->name]);
            parent::successAlert(__('general.success'));
            return redirect()->route('roles.show', $role);
        } catch (Exception $e) {
            Log::debug($e);
            return back()->withErrors(['e' => $e->getMessage()])->withInput();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param int $id
     * @return Response
     */
    public function assignPermission(int $id)
    {
        $role = Role::find($id);
        $permissions = Permission::all();
        //dd($role->permissions->pluck('id'));
        return view('roles.assign-permission-modal-form', ['role' => $role,'permissions'=>$permissions]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AssignPermissionsPost $request
     * @param int $id
     * @return Response
     */
    public function assignPermissionSubmit(AssignPermissionsPost $request,int $id)
    {
        try {
            $role = Role::find($id);
            $role->syncPermissions($request->permission_ids ?? []);

            return response()->json(
                [
                    'message' => 'Permission successfully added to role',
                    'url' => route('roles.permissions', $role),
                    'target' => '#reload-container-permissions'
                ], 200);

        } catch (Exception $e) {
            Log::debug($e);
            // return some other error, or rethrow as above.
            return response()->json(
                [
                    'message' => $e->getMessage(),
                    'url' => route('roles.permissions', $id),
                    'text' => 'client->name'
                ], 400);
        }
    }

    public function permissions(int $id)
    {
        $role = Role::find($id);
        return view('admin.roles.permissions', ['role' => $role]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param int $id
     * @param int $permission_id
     * @return Response
     */
    public function removePermission(int $id, int $permission_id)
    {
        $role = Role::find($id);
        $permission = Permission::find($permission_id);
        return view('admin.roles.remove-permission-modal-form', ['role' => $role,'permission'=>$permission]);
    }

    public function removePermissionSubmit(Request $request, int $id)
    {
        try {
            $role = Role::find($id);
            $permission = Permission::find($request->permission_id);
            $role->revokePermissionTo($permission);

            parent::successAlert(__('general.success'));

            return response()->json(
                [
                    'message' => __('client.client_create_success'),
                    'url' => route('admin.roles.permissions', $role),
                    'target' => '#reload-container-permissions'
                ], 200);

        } catch (Exception $e) {
            Log::debug($e);
            // return some other error, or rethrow as above.
            return response()->json(
                [
                    'message' => $e->getMessage(),
                    'url' => route('admin.roles.permissions', $role),
                    'text' => 'client->name'
                ], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @return Response
     */
    public function show(int $id)
    {
        $role = Role::find($id);
        return view('admin.roles.show', ['role' => $role]);
    }

    /**
     * Show the form for editing a resource.
     *
     * @return Response
     */
    public function edit(int $id)
    {
        $role = Role::find($id);
        return view('admin.roles.edit', ['role' => $role]);
    }

    /**
     * Update a resource in storage.
     *
     * @param UserPost|Request $request
     * @return Response
     */
    public function update(Request $request, int $id)
    {
        try {
            $role = Role::find($id);
            $role->name = $request->name;
            $role->save();
            parent::successAlert(__('general.success'));
            return redirect()->route('admin.roles.show', $role);
        } catch (Exception $e) {
            Log::debug($e);
            return back()->withErrors(['e' => $e->getMessage()])->withInput();
        }
    }
}
