<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use App\Services\Admin\iPermissionService;
use Spatie\Permission\Models\Permission;

class PermissionsController extends Controller
{
    protected $permissionService;

    public function __construct(iPermissionService $permissionService)
    {
        $this->permissionService = $permissionService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $permissions = Permission::all();
        return view('admin.permissions.index', ['permissions' => $permissions]);
    }

    public function list()
    {
        $permissions = Permission::all();
        return view('admin.permissions.list', ['permissions' => $permissions]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $permission = new Permission();
        return view('admin.permissions.create', ['permission' => $permission]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            $permission = Permission::create(['name' => $request->name]);
            return response()->json(
                [
                    'message' => 'Permission successfully created',
                    'url' => route('admin.permissions.list'),
                    'target' => '#reload-container-permissions'
                ], 200);
        } catch (Exception $e) {
            Log::debug($e);
            // return some other error, or rethrow as above.
            return response()->json(
                [
                    'message' => $e->getMessage(),
                    'url' => route('admin.permissions.list'),
                ], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $permission = Permission::find($id);
        return view('admin.permissions.show', ['permission' => $permission]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $permission = Permission::findById($id);
        return view('admin.permissions.edit', ['permission' => $permission]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        try {
            $permission = Permission::findById($id);
            $permission->name = $request->name;
            $permission->save();
            return response()->json(
                [
                    'message' => 'Permission successfully updated',
                    'url' => route('admin.permissions.list'),
                    'target' => '#reload-container-permissions'
                ], 200);
        } catch (Exception $e) {
            Log::debug($e);
            // return some other error, or rethrow as above.
            return response()->json(
                [
                    'message' => $e->getMessage(),
                    'url' => route('admin.permissions.list'),
                ], 400);
        }
    }

    public function delete($id)
    {
        //Permission::destroy(2);
        $permission = Permission::find($id);
        //dd()
        return view('admin.permissions.delete', ['permission' => $permission]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        try {
             Permission::destroy($id);
            return response()->json(
                [
                    'message' => 'Permission successfully deleted',
                    'url' => route('admin.permissions.list'),
                    'target' => '#reload-container-permissions'
                ], 200);
        } catch (Exception $e) {
            Log::debug($e);
            // return some other error, or rethrow as above.
            return response()->json(
                [
                    'message' => $e->getMessage(),
                    'url' => route('admin.permissions.list'),
                ], 400);
        }
    }
}
