<?php

namespace App\Http\Controllers;

use App\Http\Requests\OrderSearchPost;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{

    protected $user;

    public function __construct()
    {
        $this->user = Auth::user();
    }

    public function orders(OrderSearchPost $request)
    {
        $user = Auth::user();

        if ($user->can('admin')) {
            $orders = Order::with(['orderItems', 'customer']);

            if ($request->q) {
                $q = $request->q;
                $orders->whereHas('customer', function ($q) use ($request) {
                    $q->where('email', 'like', '%' . $request->q . '%')
                        ->orWhere('first_name', 'like', '%' . $request->q . '%')
                        ->orWhere('middle_name', 'like', '%' . $request->q . '%')
                        ->orWhere('last_name', 'like', '%' . $request->q . '%');
                })
                    ->orWhere('code', 'like', '%' . $request->q . '%');
            } else {
                $q = '';
            }

            return view('admin.orders', ['orders' => $orders->latest()->paginate(15), 'q' => $q]);
        } else {
            abort(403);
        }
    }

    public function order(int $id)
    {
        $user = Auth::user();

        if ($user->can('admin')) {
            $order = Order::with(['orderItems', 'customer'])->find($id);
            if (!$order) {
                parent::errorAlert(__('general.action_error'));
                return redirect()->route('cart.orders');
            }
            return view('admin.order', ['order' => $order]);
        } else {
            $q = '';
        }
    }
}
