<?php

namespace App\Http\Controllers\Auth;

use App\Services\iUserService;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Contracts\Auth\CanResetPassword;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use App\Http\Requests\Auth\PasswordResetPost;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/';

    use ResetsPasswords{
        reset as protected resetOverride;
    }

    protected $userService;

    /**
     * Get the password reset validation rules.
     *
     * @return array
     */
    protected function rules()
    {
        return [
            'token' => 'required',
            'email' => 'required|email',
            'password' => ['required','string','confirmed'],
        ];
    }

    /**
     * Create a new controller instance.
     *
     * ResetPasswordController constructor.
     * @param iUserService $userService
     *
     * @return void
     */
    public function __construct(iUserService $userService)
    {
        $this->middleware('guest');
        $this->userService = $userService;
    }

    /**
     * Reset the given user's password.
     *
     *
     * @param $user
     * @param string $password
     * @return void
     */
    protected function resetPassword($user, $password)
    {
        $this->userService->changeUserPassword($user->email, $password);

        event(new PasswordReset($user));

        $this->guard()->login($user);
    }

    /**
     * Reset the given user's password.
     *
     * @param PasswordResetPost $request
     * @return RedirectResponse|JsonResponse
     */
    public function reset(PasswordResetPost $request)
    {
        $response = $this->userService->passwordUsed($request['email'], $request['password']);

        if($response)
        {
            return redirect()->back()
                ->withInput($request->only('email'))
                ->withErrors(['password' => trans('passwords.history')]);
        }

        return $this->resetOverride($request);
    }
}
