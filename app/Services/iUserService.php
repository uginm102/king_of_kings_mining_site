<?php
/**
 * Created by PhpStorm.
 * User: eugene
 * Date: 07/04/2018
 * Time: 13:03
 */

namespace App\Services;


use App\Http\Requests\AssignPermissionsPost;
use App\Http\Requests\AssignRolesPost;
use App\Http\Requests\UserSearchPost;
use Illuminate\Http\Request;

interface iUserService
{
    public function getByEmail(string $email);
    public function create(string $username, string $password);
    public function list(UserSearchPost $request);
    public function getSystemUser();
    public function get(int $id);
    public function assignRole(AssignRolesPost $request,int $id);
    public function removeRole($request,int $id);
    public function assignPermission(AssignPermissionsPost $request,int $id);
    public function removePermission($request,int $id);
    public function updateEmail($email, int $id);
    public function resetPassword(int $userId);
    public function isActive(string $username);
    public function changePassword(Request $request);
    public function changeUserPassword(string $username, string $password);
    public function passwordExpired(string $username);
    public function passwordUsed(string $username, string $password);
}
