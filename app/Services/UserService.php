<?php
/**
 * Created by PhpStorm.
 * User: eugene
 * Date: 07/04/2018
 * Time: 13:03
 */

namespace App\Services;


use Carbon\Carbon;
use DB;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use App\Http\Requests\AssignPermissionsPost;
use App\Http\Requests\AssignRolesPost;
use App\Http\Requests\UserSearchPost;
use App\Models\User;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Illuminate\Http\Request;

class UserService implements iUserService
{
    protected $roleService;
    protected $lookupService;

    /**
     * UserService constructor.
     * @param iRoleService $roleService
     */
    public function __construct(iRoleService $roleService)
    {
        $this->roleService = $roleService;
    }

    public function list(UserSearchPost $request)
    {
        $users = User::with();

        $users = $users->whereHas('employee', function ($q) use ($request) {
            $q->where('email', 'like', '%' . $request->username . '%')
                ->where('is_active', true);
        });

        if ($request->q && $request->q !== '') {

            $users = $users->whereHas('employee', function ($q) use ($request) {
                $q->where('first_name', 'like', '%' . $request->q . '%')
                    ->orWhere('last_name', 'like', '%' . $request->q . '%')
                    ->orWhere('middle_name', 'like', '%' . $request->q . '%');
            });
        }

        if ($request->username && $request->username !== '') {

            $users = $users->where('email', 'like', '%' . $request->username . '%');
        }

        if ($request->job_id && $request->job_id !== 0) {

            $users = $users->whereHas('employee.employeeAssignments', function ($q) use ($request) {
                $q->where('job_id', $request->job_id);
            });
        }

        $users = $users->whereHas('employee', function ($query) {
            $query->orderBy('last_name', 'desc');
        })->paginate(15);

        return $users;
    }

    public function getSystemUser()
    {
        return User::where('email', 'like', '%system@%')->firstOrFail();
    }

    public function get(int $id)
    {
        return User::find($id);
    }

    public function assignRole(AssignRolesPost $request, int $id)
    {
        $user = $this->get($id);

        $user->syncRoles($request->role_ids ?? []);
        return $user;
    }

    public function removeRole($request, int $id)
    {
        $user = $this->get($id);
        $role = Role::find($request->role_id);

        $user->removeRole($role);
        return $user;
    }

    public function updateEmail($email, int $id)
    {
        $user = User::find($id);
        $user->email = $email;
        $user->save();
        return $user;
    }

    public function updateAction($is_active, int $id)
    {
        $user = User::find($id);
        $user->is_active = $is_active;
        $user->save();
        return $user;
    }

    public function assignPermission(AssignPermissionsPost $request, int $id)
    {
        $user = $this->get($id);

        $user->syncPermissions($request->permission_ids ?? []);
        return $user;
    }

    public function removePermission($request, int $id)
    {
        $user = $this->get($id);
        $permission = Permission::find($request->permission_id);

        $user->revokePermissionTo($permission);
        return $user;
    }

    /**
     *
     * @param int $userId
     * @return mixed
     * @deprecated
     *
     */
    public function resetPassword(int $userId)
    {
        trigger_error('Method ' . __METHOD__ . ' is deprecated', E_USER_DEPRECATED);

        $user = User::find($userId);
        $user->password = '12345678';
        //TODO removed $user->save();
        return $user;
    }

    public function isActive(string $username)
    {
        return User::where('email', $username)
            ->where('is_active', 1)->exists();
    }

    public function changePassword(Request $request)
    {
        DB::beginTransaction();
        try {

            $user = Auth::user();
            $user->password = $request->password;
            $user->save();

            $passwordSecurity = new PasswordSecurity();
            $passwordSecurity->date = Carbon::now();
            $passwordSecurity->user_id = $user->id;
            $passwordSecurity->password = $user->password;
            $passwordSecurity->save();

            DB::commit();
            return $user;
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    public function changeUserPassword(string $username, string $password)
    {
        DB::beginTransaction();
        try {

            $user = User::firstWhere('email', $username);
            $user->password = $password;
            $user->save();

            $passwordSecurity = new PasswordSecurity();
            $passwordSecurity->date = Carbon::now();
            $passwordSecurity->user_id = $user->id;
            $passwordSecurity->password = $user->password;
            $passwordSecurity->save();

            DB::commit();
            return $user;
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    public function passwordExpired(string $username)
    {
        $passwordExpiryDays = $this->lookupService->getByKey('PASSWORD_EXPIRY_DAYS');

        if(!$passwordExpiryDays) return false;

        $user = User::with(['passwordSecurities'])->firstWhere('email', $username);

        $lastPasswordSecurity = $user->passwordSecurities->sortByDesc('date')->first();

        if(!$lastPasswordSecurity) return false;

        $diff = $lastPasswordSecurity->date->diffInDays(Carbon::now());
        return $diff >=  $passwordExpiryDays->value;
    }

    public function passwordUsed(string $username, string $password)
    {
        $passwordHistoryCount = $this->lookupService->getByKey('PASSWORD_HISTORY_COUNT');

        if(!$passwordHistoryCount) return true;

        $user = User::with(['passwordSecurities'])->firstWhere('email', $username);

        $lastPasswordSecurities = $user->passwordSecurities
            ->sortByDesc('date')
            ->take($passwordHistoryCount->value);

        return $lastPasswordSecurities->contains(function ($value, $key) use ($password) {
            return Hash::check($password , $value->password);
        });
    }

    public function create(string $username, string $password)
    {
        DB::beginTransaction();
        try {

            $user = new User();
            $user->password = $password;
            $user->email = $username;
            $user->save();

            $passwordSecurity = new PasswordSecurity();
            $passwordSecurity->date = Carbon::now();
            $passwordSecurity->user_id = $user->id;
            $passwordSecurity->password = $password;
            $passwordSecurity->save();

            DB::commit();
            return $user;
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    public function getByEmail(string $email)
    {
        return User::firstWhere('email', $email);
    }
}
