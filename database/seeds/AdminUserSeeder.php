<?php

use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class AdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin_permission = Permission::firstOrCreate(['name' => 'admin']);

        $adminRole = Role::firstOrCreate(['name' => 'admin']);

        $adminRole->syncPermissions([$admin_permission]);

        $systemUser = User::firstOrCreate(['email' => 'system@example.com'],
            [
                'password' => '12345678',
                'first_name' => 'John',
                'last_name' => 'Doe',
                'telephone' => '078999',
                'address' => 'address'
            ]
        );

        $systemUser->syncRoles([$adminRole]);
    }
}
