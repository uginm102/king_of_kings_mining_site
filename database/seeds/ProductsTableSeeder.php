<?php

use App\Models\Product;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::firstOrCreate(['code' => '00001'],
            [
                'name' => 'GOLD',
                'currency' => 'USD',
                'unit' => 'Gram',
                'price' => '62.63',
                'image' => 'gold.jpg',
                'created_by' => 1,
                'created_at' => Carbon::now()]
        );

        Product::firstOrCreate(['code' => '00002'],
            [
                'name' => 'DIAMOND',
                'currency' => 'USD',
                'unit' => 'Gram',
                'price' => '300',
                'image' => 'diamond.jpg',
                'created_by' => 1,
                'created_at' => Carbon::now()]
        );

        Product::firstOrCreate(['code' => '00003'],
            [
                'name' => 'TUNGSTEN',
                'currency' => 'USD',
                'unit' => 'Kilo',
                'price' => '25',
                'image' => 'tungsten.jpg',
                'created_by' => 1,
                'created_at' => Carbon::now()]
        );

        Product::firstOrCreate(['code' => '00004'],
            [
                'name' => 'ZINC',
                'currency' => 'USD',
                'unit' => 'Ton',
                'price' => '2463.50',
                'image' => 'zinc.jpg',
                'created_by' => 1,
                'created_at' => Carbon::now()]
        );

        Product::firstOrCreate(['code' => '00005'],
            [
                'name' => 'CHROMITE',
                'currency' => 'USD',
                'unit' => 'Metric Ton',
                'price' => '449',
                'image' => 'chromite.jpg',
                'created_by' => 1,
                'created_at' => Carbon::now()]
        );

        Product::firstOrCreate(['code' => '00006'],
            [
                'name' => 'TIN',
                'currency' => 'USD',
                'unit' => 'Ton',
                'price' => '18245.00',
                'image' => 'tin.jpg',
                'created_by' => 1,
                'created_at' => Carbon::now()]
        );

        Product::firstOrCreate(['code' => '00007'],
            [
                'name' => 'IRON',
                'currency' => 'USD',
                'unit' => 'Metric Ton',
                'price' => '105.59',
                'image' => 'iron.jpg',
                'created_by' => 1,
                'created_at' => Carbon::now()]
        );

        Product::firstOrCreate(['code' => '00008'],
            [
                'name' => 'LEAD',
                'currency' => 'USD',
                'unit' => 'Ton',
                'price' => '1940',
                'image' => 'lead.jpg',
                'created_by' => 1,
                'created_at' => Carbon::now()]
        );

        Product::firstOrCreate(['code' => '00009'],
            [
                'name' => 'PLATINUM',
                'currency' => 'USD',
                'unit' => 'Gram',
                'price' => '28.87',
                'image' => 'platinum.jpg',
                'created_by' => 1,
                'created_at' => Carbon::now()]
        );

        Product::firstOrCreate(['code' => '00010'],
            [
                'name' => 'SILVER',
                'currency' => 'USD',
                'unit' => 'Gram',
                'price' => '0.87',
                'image' => 'silver.jpg',
                'created_by' => 1,
                'created_at' => Carbon::now()]
        );

        Product::firstOrCreate(['code' => '00011'],
            [
                'name' => 'MANGANESE',
                'currency' => 'USD',
                'unit' => 'Metric Ton',
                'price' => '4.5',
                'image' => 'manganese.jpg',
                'created_by' => 1,
                'created_at' => Carbon::now()]
        );
    }
}
